import { SactPage } from './app.po';

describe('sact App', () => {
  let page: SactPage;

  beforeEach(() => {
    page = new SactPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
