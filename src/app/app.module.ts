import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {Http, HttpModule, XHRBackend, RequestOptions} from '@angular/http';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import { NgModule } from '@angular/core';
import {DatePipe} from '@angular/common';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import 'hammerjs';
import { Md2Module } from 'md2';
import { FacebookModule } from 'ngx-facebook';
import { InlineSVGModule } from 'ng-inline-svg';
import { NouisliderModule } from 'ng2-nouislider';
import { CustomFormsModule } from 'ng2-validation';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { ReCaptchaModule } from 'angular2-recaptcha';
import {
  MdButtonModule,
  MdCheckboxModule,
  MdDatepickerModule,
  MdNativeDateModule,
  MdSlideToggleModule,
  MdMenuModule,
  MdInputModule,
  MdTooltipModule,
  MdRadioModule,
  MdDialogModule,
  MaterialModule, MdProgressBarModule, MdTabsModule, MdSelectModule, MdAutocompleteModule, MdChipsModule,
  MdSidenavModule
} from '@angular/material';



import { AppComponent } from './app.component';
import {AuthGuard, routing} from './app.routes';
import {
  HomeComponent,
  EventComponent,
  ProfileComponent } from './pages';
import {
  ApiService,
  CommonService,
  StoreService,
  StoreHelperService,
  SocketService,
  HttpClientService} from './services';
import {
  PlayerComponent,
  MapComponent,
  CrowdSliderComponent,
  GenderSliderComponent,
  AgeSliderComponent,
  UploadComponent,
  SliderComponent,
  OptionsComponent,
  HeaderComponent,
  EventNatureComponent,
  CommentsComponent,
  InviteUserComponent,
  EventBadgesComponent,
  UserBadgesComponent,
  RecaptchaComponent} from './elements';
import {
  CreateEventPop,
  FilterPop,
  CityFilterPop,
  LoginPop,
  NotiPop,
  CreditsPop,
  MenuPop,
  ResetPassPop,
  CreateAccPop,
  TermsPop,
  FeedbackPop,
  SendMessagePop,
  UpdateEventTopPop,
  UpdateEventBotPop,
  InviteMorePop,
  SeeAllPop,
  AddMediaPop,
  ConfirmPop,
  StatisticsPop,
  MediaSliderPop,
  LivePop,
  LiveDescPop,
  UpdateUserBotPop,
  UpdateUserTopPop,
  ParticipationPop,
  CreditsChargePop,
  ClaimMoneyPop,
  TourPop,
  TransactionsComponent} from './pop';
import {
  ScrollxDirective,
  GoogleplaceDirective,
  ShareDirective,
  PaypalDirective} from './directives';
import {Animations} from './animations/animations';
import { HtmlTransformDirective } from './directives/html-transform/html-transform.pipe';


export function TranslateLoaderFunk(http: Http) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}


export function httpClientFactory(xhrBackend: XHRBackend, requestOptions: RequestOptions): Http {
  return new HttpClientService(xhrBackend, requestOptions);
}




@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    EventComponent,
    ProfileComponent,
    PlayerComponent,
    MapComponent,
    ScrollxDirective,
    CreateEventPop,
    CityFilterPop,
    FilterPop,
    GoogleplaceDirective,
    CrowdSliderComponent,
    GenderSliderComponent,
    AgeSliderComponent,
    LoginPop,
    NotiPop,
    CreditsPop,
    MenuPop,
    ResetPassPop,
    CreateAccPop,
    TermsPop,
    FeedbackPop,
    SendMessagePop,
    UpdateEventTopPop,
    UpdateEventBotPop,
    InviteMorePop,
    SeeAllPop,
    AddMediaPop,
    ConfirmPop,
    StatisticsPop,
    UploadComponent,
    SliderComponent,
    MediaSliderPop,
    OptionsComponent,
    HeaderComponent,
    EventNatureComponent,
    LivePop,
    LiveDescPop,
    UpdateUserBotPop,
    UpdateUserTopPop,
    CommentsComponent,
    ParticipationPop,
    InviteUserComponent,
    ShareDirective,
    EventBadgesComponent,
    UserBadgesComponent,
    PaypalDirective,
    CreditsChargePop,
    ClaimMoneyPop,
    TourPop,
    RecaptchaComponent,
    TransactionsComponent,
    HtmlTransformDirective
  ],
  entryComponents: [
    CreateEventPop,
    CityFilterPop,
    FilterPop,
    LoginPop,
    NotiPop,
    CreditsPop,
    MenuPop,
    ResetPassPop,
    CreateAccPop,
    TermsPop,
    FeedbackPop,
    ConfirmPop,
    StatisticsPop,
    AddMediaPop,
    LivePop,
    LiveDescPop,
    UpdateEventTopPop,
    UpdateEventBotPop,
    CreateEventPop,
    UpdateUserBotPop,
    UpdateUserTopPop,
    MediaSliderPop,
    SendMessagePop,
    ParticipationPop,
    InviteMorePop,
    SeeAllPop,
    CreditsChargePop,
    ClaimMoneyPop,
    TourPop,
    TransactionsComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    FormsModule,
    HttpModule,
    routing,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: TranslateLoaderFunk,
        deps: [Http]
      }
    }),
    BrowserAnimationsModule,
    
    MdChipsModule,
    MdAutocompleteModule,
    MdSelectModule,
    MdTabsModule,
    MdProgressBarModule,
    MdNativeDateModule,
    MdDialogModule,
    MdButtonModule,
    MdCheckboxModule,
    MdSlideToggleModule,
    MdDatepickerModule,
    MdMenuModule,
    MdInputModule,
    MdTooltipModule,
    MdRadioModule,
    MdSidenavModule,
    
    ReactiveFormsModule,
    NouisliderModule,
    InlineSVGModule,
    Md2Module,
    CustomFormsModule,
    FacebookModule.forRoot(),
    InfiniteScrollModule,
    ChartsModule,
    ReCaptchaModule
  ],
  providers: [
    ApiService,
    CommonService,
    StoreService,
    SocketService,
    StoreHelperService,
    DatePipe,
    AuthGuard,
    Animations,
    { provide: Http, useFactory: httpClientFactory, deps: [XHRBackend, RequestOptions] }
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }



