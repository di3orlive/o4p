import {Component, OnInit} from '@angular/core';
import {CommonService} from '../../services';
import {ApiService} from "../../services/api/api.service";
import {SafeSubscribe} from "../../helpers/safe-subscripe/safe-subscripe";

declare const YT;

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss']
})
export class PlayerComponent extends SafeSubscribe implements OnInit {
  player: any;
  controls: any;
  parameters: any;

  constructor(
    private commonService: CommonService,
    private api: ApiService
  ) {
    super();
    this.commonService.controlsAsync.safeSubscribe(this, (value) => {
      this.controls = value;

      this.playerControls();
    });
  }

  
  ngOnInit() {
    if (innerWidth > 1023) {
      this.commonService.parametersAsync.safeSubscribe(this, (value) => {
        this.parameters = value.parameters;
        
        if (this.parameters && this.parameters.youtube_video) {
          this.onYouTubeIframeAPIReady(this.parameters.youtube_video);
        }
      });
    }
  }


  onYouTubeIframeAPIReady(videoId) {
    if (!navigator.onLine) { return null; }

    const checkMap = setInterval(() => {
      if ((typeof YT !== 'undefined') && YT && YT.Player) {
        clearInterval(checkMap);
        this.player = new YT.Player('video', {
          width: 600,
          height: 400,
          videoId: videoId,
          playerVars: {
            controls: 0,
            autoplay: 1,
            showinfo: 0,
            rel: 0,
            playlist: videoId,
            loop: 1
          },
          events: {
            'onReady': function (event) {
              event.target.setVolume(0);
            }
          }
        });
      }
    }, 10);
  }

  playerControls() {
    if (this.player) {

      if (this.controls.isMuted) {
        this.player.setVolume(0);
      } else {
        this.player.setVolume(100);
      }


      if (this.controls.isPaused) {
        this.player.pauseVideo();
      }else {
        this.player.playVideo();
      }
    }
  }
}
