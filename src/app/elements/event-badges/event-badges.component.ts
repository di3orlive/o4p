import {Component, OnInit, Input, EventEmitter, Output} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {Md2Toast} from 'md2';
import {SafeSubscribe} from '../../helpers/safe-subscripe/safe-subscripe';
import {ApiService} from '../../services';

@Component({
  selector: 'app-event-badges',
  templateUrl: './event-badges.component.html',
  styleUrls: ['./event-badges.component.scss']
})
export class EventBadgesComponent extends SafeSubscribe implements OnInit {
  @Input() event: any;
  @Output() onVote = new EventEmitter();
  badges = [
    {id: 'sausage', tooltip: '', isActive: false},
    {id: 'classic', tooltip: '', isActive: false},
    {id: 'time', tooltip: '', isActive: false}
  ];


  constructor(
    private toast: Md2Toast,
    private translate: TranslateService,
    private api: ApiService
  ) {
    super();
  }


  ngOnInit() {
  }


  addVote(id) {
    if (!this.event.can_vote) { return null; }

    const body = {
      badge_type_id: id
    };

    this.api.postPB(`events/${this.event.secure_id}/badges/`, body).safeSubscribe(this, (res: any) => {
      if (res.ok) {
        this.onVote.emit('YO');
        this.toast.toast(this.translate.instant('you_have_voted'));
      }
    });
  }
}
