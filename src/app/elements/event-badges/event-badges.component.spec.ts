import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventBadgesComponent } from './event-badges.component';

describe('EventBadgesComponent', () => {
  let component: EventBadgesComponent;
  let fixture: ComponentFixture<EventBadgesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventBadgesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventBadgesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
