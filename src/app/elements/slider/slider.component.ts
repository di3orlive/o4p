import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {MdDialog, MdDialogConfig, MdDialogRef} from '@angular/material';
import {SafeSubscribe} from '../../helpers/safe-subscripe/safe-subscripe';
import {AddMediaPop, MediaSliderPop} from '../../pop';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent extends SafeSubscribe implements OnInit {
  addMediaDialogRef: MdDialogRef<AddMediaPop>;
  mediaSliderDialogRef: MdDialogRef<MediaSliderPop>;
  @Input() slider: any;
  @Output() onMediaAdded = new EventEmitter();
  tooltip: any;


  constructor(
    private translate: TranslateService,
    public dialog: MdDialog
  ) {
    super();
  }

  ngOnInit() {
    this.initTooltip();
  }


  addMediaPop() {
    if (!this.slider.can_add_medias) { return null; }

    this.addMediaDialogRef = this.dialog.open(AddMediaPop, <MdDialogConfig>{
      width: '645px'
    });

    this.addMediaDialogRef.afterClosed().safeSubscribe(this, res => {
      if (!!res) {
        this.onMediaAdded.emit(res);
      }
    });
  }


  mediaSliderPop(index) {
    this.mediaSliderDialogRef = this.dialog.open(MediaSliderPop, <MdDialogConfig>{
      width: '86%',
      height: '86%',
      data: {
        mediaArr: this.slider.medias,
        mediaIndex: index
      }
    });

    this.mediaSliderDialogRef.afterClosed().safeSubscribe(this, res => {
      if (!!res) {
      }
    });
  }


  initTooltip() {
    if (this.slider.event_nature) {
      this.tooltip = this.slider.can_add_medias ? this.translate.instant('you_can_post_media') : this.translate.instant('you_need_to_be_a_validated_participant_to_post_an_image');
    } else {
      this.tooltip = this.slider.can_add_medias ? this.translate.instant('you_can_post_media') : this.translate.instant('you_need_to_share_an_event_with_that_user_firs');
    }
  }
}
