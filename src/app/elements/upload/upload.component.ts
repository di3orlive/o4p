import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit {
  @Input() file: any;
  @Input() isMultiple: any;
  @Output() onFile = new EventEmitter();
  allowFileDrop: any;


  constructor() { }


  ngOnInit() {
    this.onFile.emit(this.file);
  }


  allowDrop(event): void {
    event.preventDefault();
    this.allowFileDrop = true;
  }


  onFileDrop(event) {
    event.preventDefault();
    this.allowFileDrop = false;
    this.uploadResume(event.dataTransfer.files);
  }


  onFileChange(event) {
    this.uploadResume(event.target.files);
  }


  uploadResume(files: FileList) {
    if (files.length > 0) {
      for (let i = 0; i < files.length; i++) {

        const reader = new FileReader();

        reader.onloadend = () => {
          this.file.arr.push({file: files[i], img: reader.result});

          this.onFile.emit(this.file);
        };

        reader.readAsDataURL(files[i]);
      }
    }
  }
}


