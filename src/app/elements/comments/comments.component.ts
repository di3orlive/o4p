import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {Md2Toast} from 'md2';
import {SafeSubscribe} from '../../helpers/safe-subscripe/safe-subscripe';
import {ApiService} from '../../services';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss']
})
export class CommentsComponent extends SafeSubscribe implements OnInit {
  @Input() comments: any;
  @Input() can_comment: any;
  @Input() comment_path: any;
  @Input() secure_id: any;
  @Output() on_comment = new EventEmitter();
  comment = {
    val: ''
  };

  constructor(
    private api: ApiService,
    private translate: TranslateService,
    private toast: Md2Toast
  ) {
    super();
  }

  ngOnInit() {
  }


  addComment(comment) {
    if (!comment) {
      return null;
    }

    const body = {
      comment: comment
    };

    this.api.postPB(`${this.comment_path}/${this.secure_id}/comments`, body).safeSubscribe(this, (res) => {
      this.comment.val = null;
      this.on_comment.emit();
      this.toast.toast(this.translate.instant('comment_created'));
    });
  }


  removeComment(comment, can_comment) {
    if (can_comment) {
      this.api.deleteP(`${this.comment_path}/comments/${comment.comment_secure_id}`).safeSubscribe(this, (res) => {
        this.on_comment.emit();
        this.toast.toast(this.translate.instant('comment_deleted'));
      });
    } else {
      this.toast.toast(this.translate.instant('you_need_to_be_a_participant_to_comment'));
    }
  }
}
