import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-age-slider',
  templateUrl: './age-slider.component.html',
  styleUrls: ['./age-slider.component.scss']
})
export class AgeSliderComponent implements OnInit {
  @Input() filter: any = {
    isActive: true,
    val: [0, 100]
  };
  @Output() onSlide = new EventEmitter();

  config = {
    start: this.filter.val,
    connect: true,
    step: 1,
    tooltips: [true, true],
    range: {
      min: 0,
      max: 100
    },
    format: {
      to: (value) => {
        return Math.round(value);
      },
      from: function (value) {
        return Math.round(value);
      }
    }
  };

  constructor() {
  }

  ngOnInit() {
    this.onSlideEmit();
  }

  onSlideEmit() {
    this.onSlide.emit(this.filter);
  }
}
