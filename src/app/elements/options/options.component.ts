import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {SafeSubscribe} from '../../helpers/safe-subscripe/safe-subscripe';
import {CommonService} from '../../services';

@Component({
  selector: 'app-options',
  templateUrl: './options.component.html',
  styleUrls: ['./options.component.scss']
})
export class OptionsComponent extends SafeSubscribe implements OnChanges, OnInit {
  @Input() inputOptions: any;
  @Input() disableToggle: any;
  @Input() isFilter = false;
  @Output() onOption = new EventEmitter();
  options = [];

  constructor(
    private commonService: CommonService
  ) {
    super();
    this.commonService.optionsAsync.safeSubscribe(this, () => {
      this.options = this.commonService.getDefaultOptions();
    });
  }


  ngOnInit() {
    this.initOptions();
    this.onOption.emit(this.options);
  }

  ngOnChanges() {
    this.initOptions();
  }


  initOptions() {
    if (this.inputOptions) {
      this.options = [];
      this.inputOptions.forEach((it) => {
        this.options.push(Object.assign({}, it));
      });
    }
  }


  toggleOption(i) {
    if (this.disableToggle) { return null; }

    i.isactive = !i.isactive;

    this.onOption.emit(this.options);
  }
}


