import {Component, OnInit} from '@angular/core';
import {SafeSubscribe} from '../../helpers/safe-subscripe/safe-subscripe';
import {ApiService, CommonService} from '../../services';

declare const google;
declare const noUiSlider;


@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent extends SafeSubscribe implements OnInit {
  map: any;
  mapStyles = [
    [
      {
        'featureType': 'administrative',
        'elementType': 'labels.text',
        'stylers': [
          {
            'visibility': 'off'
          }
        ]
      },
      {
        'featureType': 'administrative.province',
        'elementType': 'geometry',
        'stylers': [
          {
            'visibility': 'off'
          }
        ]
      },
      {
        'featureType': 'landscape',
        'elementType': 'all',
        'stylers': [
          {
            'color': '#f2f2f2'
          }
        ]
      },
      {
        'featureType': 'poi',
        'elementType': 'all',
        'stylers': [
          {
            'visibility': 'off'
          }
        ]
      },
      {
        'featureType': 'road',
        'elementType': 'all',
        'stylers': [
          {
            'saturation': -100
          },
          {
            'lightness': 45
          }
        ]
      },
      {
        'featureType': 'road.highway',
        'elementType': 'all',
        'stylers': [
          {
            'visibility': 'off'
          }
        ]
      },
      {
        'featureType': 'road.arterial',
        'elementType': 'labels.icon',
        'stylers': [
          {
            'visibility': 'off'
          }
        ]
      },
      {
        'featureType': 'transit',
        'elementType': 'all',
        'stylers': [
          {
            'visibility': 'off'
          }
        ]
      },
      {
        'featureType': 'water',
        'elementType': 'all',
        'stylers': [
          {
            'color': '#000000'
          },
          {
            'visibility': 'on'
          }
        ]
      }
    ],
    [
      {
        'featureType': 'administrative',
        'elementType': 'labels.text.fill',
        'stylers': [
          {
            'color': '#444444'
          },
          {
            'visibility': 'on'
          }
        ]
      },
      {
        'featureType': 'landscape',
        'elementType': 'all',
        'stylers': [
          {
            'color': '#f2f2f2'
          }
        ]
      },
      {
        'featureType': 'poi',
        'elementType': 'all',
        'stylers': [
          {
            'visibility': 'off'
          }
        ]
      },
      {
        'featureType': 'road',
        'elementType': 'all',
        'stylers': [
          {
            'saturation': -100
          },
          {
            'lightness': 45
          }
        ]
      },
      {
        'featureType': 'road.highway',
        'elementType': 'all',
        'stylers': [
          {
            'visibility': 'off'
          }
        ]
      },
      {
        'featureType': 'road.arterial',
        'elementType': 'labels.icon',
        'stylers': [
          {
            'visibility': 'off'
          }
        ]
      },
      {
        'featureType': 'transit',
        'elementType': 'all',
        'stylers': [
          {
            'visibility': 'off'
          }
        ]
      },
      {
        'featureType': 'water',
        'elementType': 'all',
        'stylers': [
          {
            'color': '#000000'
          },
          {
            'visibility': 'on'
          }
        ]
      }
    ]
  ];
  mapOptions = {
    center: {lat: 48, lng: 22},
    disableDefaultUI: true,
    zoom: 5,
    minZoom: 3,
    maxZoom: 14,
    styles: this.mapStyles[0]
  };
  mapMarkers = [];
  slider: any;
  sliderOptions = {
    start: [5],
    orientation: 'vertical',
    range: {
      'min': 3,
      'max': 14
    }
  };


  constructor(
    private commonService: CommonService,
    private api: ApiService
  ) {
    super();
  }


  ngOnInit() {
    if (innerWidth > 1023) {
      this.api.getP(`markers`).safeSubscribe(this, (resp: any) => {
        this.mapMarkers = resp.markers;
    
        this.initMap();
        this.initSlider();
      });
      
      return;
    }
    
    this.initMap();
    this.initSlider();
  }


  initMap() {
    this.map = new google.maps.Map(document.querySelector('.map'), this.mapOptions);
    const service = new google.maps.places.PlacesService(this.map);


    this.commonService.setMap(this.map, service);


    for (let i = 0; i < this.mapMarkers.length; i++) {
      setTimeout(() => {
        const marker = new google.maps.Marker({
          position: new google.maps.LatLng(this.mapMarkers[i].gps_latitude, this.mapMarkers[i].gps_longitude),
          map: this.map,
          icon: this.mapMarkers[i].image,
          animation: google.maps.Animation.DROP
        });

        const infowindow = new google.maps.InfoWindow({
          content: this.mapMarkers[i].label
        });
        marker.addListener('click', () => {
          this.map.setZoom(10);
          this.map.setCenter(marker.getPosition());
          this.map.panBy(this.commonService.mapPanBy, 0);
        });
  

        marker.addListener('mouseover', () => {
          infowindow.open(this.map, marker);
        });

        marker.addListener('mouseout', () => {
          infowindow.close();
        });


      }, i * 200);
    }


    this.map.addListener('zoom_changed', () => {
      this.changeZoomBtn(0);

      if (this.map.getZoom() > 6) {
        this.map.setOptions({styles: this.mapStyles[1]});
      } else {
        this.map.setOptions({styles: this.mapStyles[0]});
      }
    });


    this.map.addListener('idle', () => {
      setTimeout(() => {
        this.setMapCenter();
        this.commonService.setMap(this.map, service);
      }, 1000);
    });
  }


  initSlider() {
    this.slider = document.querySelector('.slider-box');

    noUiSlider.create(this.slider, this.sliderOptions);

    this.slider.noUiSlider.on('update', ( values, handle ) => {
      const curVal = Math.floor(values[handle]);
      if ( handle === 0 ) {
        if (curVal !== this.map.getZoom()) {
          this.map.setZoom(curVal);
        }
      }
    });
  }


  changeZoomBtn(num) {
    this.slider.noUiSlider.set([this.map.getZoom() + num]);
  }
  
  
  setMapCenter() {
    let loc = this.map.getCenter();
    loc = new google.maps.LatLng(loc.lat(), loc.lng(), false);
    this.map.setCenter(loc);
    // this.map.setCenter(marker.getPosition());
  }
}










