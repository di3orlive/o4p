import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-recaptcha',
  templateUrl: './recaptcha.component.html',
  styleUrls: ['./recaptcha.component.scss']
})
export class RecaptchaComponent {
  @Output() succcess = new EventEmitter();
  recaptchaSiteKey = '6LcDNC0UAAAAAFKvD0JLtDBu8n2qr2eqLteOC-4U';
  
  constructor() {
  }
  
  
  handleCorrectCaptcha(e) {
    this.succcess.emit(e)
  }
}
