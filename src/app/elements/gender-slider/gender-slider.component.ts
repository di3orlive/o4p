import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-gender-slider',
  templateUrl: './gender-slider.component.html',
  styleUrls: ['./gender-slider.component.scss']
})
export class GenderSliderComponent implements OnInit {
  @Input() filter: any = {
    isActive: true,
    val: 0,
  };
  @Output() onSlide = new EventEmitter();

  config = {
    start: this.filter.val,
    connect: true,
    step: 1,
    tooltips: true,
    range: {
      min: 0,
      max: 4
    },
    format: {
      to: (value) => {
        let out = value;

        switch (value) {
          case 0: out = this.translate.instant('only_men'); break;
          case 1: out = this.translate.instant('more_men'); break;
          case 2: out = this.translate.instant('balanced'); break;
          case 3: out = this.translate.instant('more_women'); break;
          case 4: out = this.translate.instant('only_women'); break;
        }

        return out;
      },
      from: (value) => {
        let out = value;

        switch (this.translate.instant(value)) {
          case this.translate.instant('only_men'): out = 0; break;
          case this.translate.instant('more_men'): out = 1; break;
          case this.translate.instant('balanced'): out = 2; break;
          case this.translate.instant('more_women'): out = 3; break;
          case this.translate.instant('only_women'): out = 4; break;
        }

        return out;
      }
    }
  };

  constructor(private translate: TranslateService) {
  }

  ngOnInit() {
    this.onSlideEmit();
  }

  onSlideEmit() {
    this.onSlide.emit(this.filter);
  }
}
