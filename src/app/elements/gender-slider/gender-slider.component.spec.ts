import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenderSliderComponent } from './gender-slider.component';

describe('GenderSliderComponent', () => {
  let component: GenderSliderComponent;
  let fixture: ComponentFixture<GenderSliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenderSliderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenderSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
