import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {Md2Toast} from 'md2';
import {SafeSubscribe} from '../../helpers/safe-subscripe/safe-subscripe';
import {ApiService} from '../../services';

@Component({
  selector: 'app-invite-user',
  templateUrl: './invite-user.component.html',
  styleUrls: ['./invite-user.component.scss']
})
export class InviteUserComponent extends SafeSubscribe implements OnInit {
  @Output() onUser = new EventEmitter();
  user: any;
  users = [];
  usersArr = [];
  email_pattern = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;


  constructor(
    private toast: Md2Toast,
    private translate: TranslateService,
    private api: ApiService
  ) {
    super();
  }


  ngOnInit() {
  }


  findUser(user) {
    if (this.api.O4PUser.loggedIn && user.length > 2) {
    console.log(this);
      this.api.getP(`usernames?input=${user}`).safeSubscribe(this, (res: any) => {
        if (Object.keys(res.usernames).length === 0 && res.usernames.constructor === Object) { return null; }

        const una = res.usernames;
        this.usersArr = [];
        for (let i = 0; i < una.length; i++) {
          this.usersArr.push({username: `${una[i].username}`});
        }
      });
    }


    if (user.indexOf('@') > 0) {
      if (user.match(this.email_pattern)) {
        this.usersArr.length = 0;
        this.usersArr.push({username: user});
      }
    }
  }

  addUser(user) {
    if (user.username.indexOf('@') > 0) {
      if (!user.username.match(this.email_pattern)) {
        this.toast.toast(this.translate.instant('invalid_email'));
        return null;
      }
    }

    if (this.users.indexOf(user) < 0) {
      this.users.push(user);
      this.usersArr.length = 0;
      this.user = '';

      this.onUser.emit(this.users);
    }
  }

  removeUser(i) {
    this.users.splice(i, 1);

    this.onUser.emit(this.users);
  }
}



