import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {Md2Toast} from 'md2';
import {SafeSubscribe} from '../../helpers/safe-subscripe/safe-subscripe';
import {ApiService} from '../../services';

@Component({
  selector: 'app-user-badges',
  templateUrl: './user-badges.component.html',
  styleUrls: ['./user-badges.component.scss']
})
export class UserBadgesComponent extends SafeSubscribe implements OnInit {
  @Input() user: any;
  @Output() onVote = new EventEmitter();
  badges = [
    {id: 'loser', tooltip: '', isActive: false},
    {id: 'player', tooltip: '', isActive: false},
    {id: 'cool', tooltip: '', isActive: false}
  ];


  constructor(
    private toast: Md2Toast,
    private translate: TranslateService,
    private api: ApiService
  ) {
    super();
  }


  ngOnInit() {
  }


  addVote(id) {
    if (!this.user.can_vote) { return null; }

    const body = {
      badge_type_id: id
    };

    this.api.postPB(`users/${this.user.secure_id}/badges/`, body).safeSubscribe(this, (res) => {
      console.log(res);
      this.onVote.emit('YO');
      this.toast.toast(this.translate.instant('you_have_voted'));
    });
  }
}
