import {Component, Input, OnInit} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {MdDialog, MdDialogConfig, MdDialogRef} from '@angular/material';
import {SafeSubscribe} from '../../helpers/safe-subscripe/safe-subscripe';
import {NotiPop, CreditsPop} from '../../pop';
import {CommonService} from '../../services';



@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent extends SafeSubscribe implements OnInit {
  @Input() sidenav: any;
  notiPopRef: MdDialogRef<NotiPop>;
  creditsPopRef: MdDialogRef<CreditsPop>;
  O4PUser: any;
  controls: any;
  isHomePage: any;

  constructor(
    public dialog: MdDialog,
    private router: Router,
    private commonService: CommonService
  ) {
    super();
    this.commonService.userAsync.safeSubscribe(this, (res) => {
      this.O4PUser = res;
    });


    this.commonService.controlsAsync.safeSubscribe(this, (res) => {
      this.controls = res;
    });


    this.router.events.safeSubscribe(this, (res) => {
      if (res instanceof NavigationEnd) {
        this.isHomePage = res.url === '/';
      }
    });
  }


  ngOnInit() {
  }


  menuPop() {
    this.sidenav.open();
    // this.menuPopRef = this.dialog.open(MenuPop, {
    //   position: { top: '0', right: '0' },
    //   height: '100%'
    // });
  }


  notiPop() {
    this.notiPopRef = this.dialog.open(NotiPop, <MdDialogConfig>{
      position: {top: '30px', right: '30px'},
      height: '300px',
      width: '300px'
    });
  }


  creditsPop() {
    this.creditsPopRef = this.dialog.open(CreditsPop, <MdDialogConfig>{
      position: {top: '30px', right: '30px'}
    });
  }



  toggleVideo() {
    this.commonService.toggleVideo();
  }

  toggleSound() {
    this.commonService.toggleSound();
  }
}
