import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrowdSliderComponent } from './crowd-slider.component';

describe('CrowdSliderComponent', () => {
  let component: CrowdSliderComponent;
  let fixture: ComponentFixture<CrowdSliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrowdSliderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrowdSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
