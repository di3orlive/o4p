import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-crowd-slider',
  templateUrl: './crowd-slider.component.html',
  styleUrls: ['./crowd-slider.component.scss']
})
export class CrowdSliderComponent implements OnInit {
  @Input() filter: any = {
    isActive: true,
    val: [0, 50]
  };
  @Output() onSlide = new EventEmitter();

  min = 0;
  max = 100;
  range: any;


  constructor() {
  }


  ngOnInit() {
    this.range = this.filter.val;
    this.onSlideEmit();
  }

  onSlideEmit() {
    this.onSlide.emit({
      isActive: this.filter.isActive,
      val: this.range
    });
  }

  onSlideChange(e) {
    const max = e[1];

    if (max === this.max) {
      this.max *=  2;
    } else if (max === this.min) {
      this.max /=  2;
      this.range[1] = this.max - 2; // not working
    }
  
    this.range = e;
    this.onSlideEmit();
  }
}
