import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-event-nature',
  templateUrl: './event-nature.component.html',
  styleUrls: ['./event-nature.component.scss']
})
export class EventNatureComponent implements OnChanges, OnInit {
  @Input() inputVal: any;
  @Input() initValue: any;
  @Input() disableSet: any;
  @Input() isFilter: any;
  @Output() onSet = new EventEmitter();
  val = 'fiesta';
  arr = [
    {name: 'fiesta', isactive: false},
    {name: 'sport', isactive: false},
    {name: 'business', isactive: false}
  ];


  constructor(
  ) {
  }


  ngOnInit() {
    if (this.isFilter) { return null; }
  }


  ngOnChanges() {
    if (this.isFilter) { return null; }

    if (this.inputVal) {
      setTimeout(() => {
        this.arr.length = 0;
        this.inputVal.forEach((it) => {
          this.arr.push(Object.assign({}, it));
        });
  
        this.val = null;
        this.arr.forEach((it) => {
          if (it.isactive) {
            this.set(it);
          }
        });
      }, 0);
    }

    if (this.initValue) {
      this.arr.forEach((it) => {
        if (it.name === this.initValue) {
          this.set(it);
        }
      });
    }
  }


  set(i) {
    if (this.disableSet) { return null; }

    if (i.isactive && this.isFilter) {
      i.isactive = false;
      this.onSet.emit(this.arr);
      return;
    }

    this.arr.forEach((it) => {
      it.isactive = false;
    });

    i.isactive = true;

    this.onSet.emit(this.arr);
  }
}
