import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventNatureComponent } from './event-nature.component';

describe('EventNatureComponent', () => {
  let component: EventNatureComponent;
  let fixture: ComponentFixture<EventNatureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventNatureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventNatureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
