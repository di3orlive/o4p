import {Component, OnInit, AfterViewInit, HostBinding} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {Router} from '@angular/router';
import {MdDialog, MdDialogConfig, MdDialogRef} from '@angular/material';
import {Md2Toast} from 'md2';
import {Animations} from '../../animations/animations';
import {SafeSubscribe} from '../../helpers/safe-subscripe/safe-subscripe';
import {FilterPop, CityFilterPop, LoginPop, CreateEventPop, TourPop} from '../../pop/';
import {ApiService, StoreService, CommonService} from '../../services';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: Animations.page
})
export class HomeComponent extends SafeSubscribe implements OnInit, AfterViewInit {
  @HostBinding('@routeAnimation') routeAnimation = true;
  
  createEventDialogRef: MdDialogRef<CreateEventPop>;
  filterDialogRef: MdDialogRef<FilterPop>;
  cityFilterDialogRef: MdDialogRef<CityFilterPop>;
  logInPopRef: MdDialogRef<LoginPop>;
  tourPopRef: MdDialogRef<TourPop>;
  filter = {
    options: [],
    event_nature: [],
    city: {
      isActive: false,
      full_address: '',
      place_id: '',
      gpsLatitude: 0,
      gpsLongitude: 0
    },
    more: {
      isActive: false,
      crowd: {
        isActive: false,
        val: [0, 50]
      },
      gender: {
        isActive: false,
        val: 0
      },
      age: {
        isActive: false,
        val: [0, 100]
      }
    },
    predefinedFilter: null,
    mapZoom: 10,
    pageNb: 1,
  };
  O4PUser: any;
  map: any;
  events = [];
  mobileMenu = false;

  constructor(
    private router: Router,
    public dialog: MdDialog,
    private commonService: CommonService,
    private api: ApiService,
    private store: StoreService,
    private toast: Md2Toast,
    private translate: TranslateService,
  ) {
    super();
    this.commonService.userAsync.safeSubscribe(this, (value) => {
      this.O4PUser = value;
    });
  
  
    this.tourPop();
  }


  ngOnInit() {
    this.findPanBy();

    this.commonService.mapAsync.safeSubscribe(this, (mapres) => {
      if (!mapres.map || this.filter.city.gpsLatitude === mapres.map.center.lat() || this.filter.city.gpsLongitude === mapres.map.center.lng()) { return null; }
      this.map = mapres.map;
      this.getEvents({});
    });
  }
  
  
  ngAfterViewInit() {
    // setTimeout(() => {
    //   if (!localStorage.getItem('O4PIntroHome')) {
    //     localStorage.setItem('O4PIntroHome', JSON.stringify(true));
    //
    //     this.commonService.introJs.setOptions({
    //       showStepNumbers: false,
    //       tooltipPosition: 'auto',
    //       steps: [
    //         {
    //           element: document.getElementById('home-intro-one'),
    //           intro: 'First step will be to create an account and login'
    //         },
    //         {
    //           // element: document.getElementById('home-intro-two'),
    //           intro: 'then use the map to locate events, use the markers to find them easily'
    //         },
    //         {
    //           element: document.getElementById('home-intro-three'),
    //           intro: 'or use your current location'
    //         },
    //         {
    //           element: document.getElementById('home-intro-four'),
    //           intro: 'use these buttons to filter the results'
    //         },
    //         {
    //           // element: document.getElementById('home-intro-five'),
    //           intro: 'finally choose an event here and click on it'
    //         },
    //         {
    //           element: document.getElementById('home-intro-six'),
    //           intro: 'you didn’t find an event that matches your expectations ? then make yours'
    //         },
    //         {
    //           element: document.getElementById('home-intro-seven'),
    //           intro: 'don’t miss what happens with the notification system'
    //         }
    //       ]
    //     });
    //
    //     this.commonService.introJs.start();
    //   }
    // }, 5000);
  }


  getEvents(bodyIn: any) {
    if (this.map) {
      this.filter.city.gpsLatitude = this.map.center.lat();
      this.filter.city.gpsLongitude = this.map.center.lng();
    }
    
    
    const body = {
      predefinedFilter: bodyIn.predefinedFilter || this.filter.predefinedFilter,
      gpsLatitude: this.map ? this.map.center.lat() : 48,
      gpsLongitude: this.map ? this.map.center.lng() : 22,
      mapZoom: this.map ? this.map.getZoom() : 5,
      pageNb: 1,
      options: bodyIn.options || this.filter.options[0] != null ? this.filter.options : [],
      genderBalance: bodyIn.genderBalance || this.filter.more.isActive && this.filter.more.gender.isActive ? this.filter.more.gender.val : null,
      ageMin: bodyIn.ageMin || this.filter.more.isActive && this.filter.more.age.isActive ? this.filter.more.age.val[0] : null,
      ageMax: bodyIn.ageMax || this.filter.more.isActive && this.filter.more.age.isActive ? this.filter.more.age.val[1] : null,
      nbMin: bodyIn.nbMin || this.filter.more.isActive && this.filter.more.crowd.isActive ? this.filter.more.crowd.val[0] : null,
      nbMax: bodyIn.nbMax || this.filter.more.isActive && this.filter.more.crowd.isActive ? this.filter.more.crowd.val[1] : null,
      event_nature: bodyIn.event_nature || this.filter.event_nature || null
    };


    this.api.getEvents(body).safeSubscribe(this);


    this.store.changes.map(changesres => changesres.events).safeSubscribe(this, (eventsres) => {
      if (!eventsres) { return null; }
      this.events = eventsres.events;

      // fix ng for of obj err
      if (Object.keys(this.events).length === 0 && this.events.constructor === Object) {
        this.events = [];
      }


      for (const event of this.events) {
        const city = this.commonService.getCityByPlaceId(event.google_place_id);

        if (!city) {
          this.commonService.getInfoFromPlaceId(event.google_place_id).safeSubscribe(this, (res: any) => {
            event.city_country = res.city_country;
            event.full_address = res.full_address;
            this.commonService.pushCity(res);
          });
        } else {
          event.city_country = city.city_country;
          event.full_address = city.full_address;
        }
      }
    });
  }


  onOption(e) {
    this.filter.options = [];

    for (let i = 0; i < e.length; i++) {
      if (e[i].isactive) {
        this.filter.options.push(e[i].name);
      }
    }

    this.getEvents({
      options: this.filter.options
    });
  }


  onNatureSet(arr) {
    let event_nature = '';

    arr.forEach((it) => {
      if (it.isactive) {
        event_nature = it.name;
      }
    });

    this.getEvents({
      event_nature: event_nature
    });
  }


  openFilterMore() {
    this.filterDialogRef = this.dialog.open(FilterPop, <MdDialogConfig>{
      data: this.filter.more,
    });

    this.filterDialogRef.afterClosed().safeSubscribe(this, res => {
      console.log(res);
      if (!!res) {
        this.filter.more = res;

        const body = {
          genderBalance: res.isActive && res.gender.isActive ? res.gender.val : null,
          ageMin: res.isActive && res.age.isActive ? res.age.val[0] : null,
          ageMax: res.isActive && res.age.isActive ? res.age.val[1] : null,
          nbMin: res.isActive && res.crowd.isActive ? res.crowd.val[0] : null,
          nbMax: res.isActive && res.crowd.isActive ? res.crowd.val[1] : null
        };

        this.getEvents(body);
      }
    });
  }


  cityFilterPop() {
    this.cityFilterDialogRef = this.dialog.open(CityFilterPop, <MdDialogConfig>{
      position: {top: '50px', left: '43%'},
      data: this.filter.city
    });

    this.cityFilterDialogRef.afterClosed().safeSubscribe(this, res => {
      if (!!res) {
        this.filter.city = res;

        if (res.isActive) {
          this.map.setZoom(10);
          this.map.setCenter({lat: res.gpsLatitude, lng: res.gpsLongitude});
          this.map.panBy(this.commonService.mapPanBy, 0);

          this.getEvents({});
        }
      }
    });
  }


  predefinedFilterFunc(f) {
    this.filter.predefinedFilter = f;


    if (f === 'around') {
      this.predefinedAround();
    } else if (f === 'reset') {
      this.predefinedReset();
    } else {
      this.getEvents({});
    }
  }


  predefinedAround() {
    if (navigator.geolocation) {
      const setGps = (lat, lon) => {

        this.map.setZoom(10);
        this.map.setCenter({lat: lat, lng: lon});
        this.map.panBy(this.commonService.mapPanBy, 0);

        this.getEvents({});
      };

      const onUserDenied = () => {
        this.filter.predefinedFilter = null;
        this.toast.toast(this.translate.instant('you_have_blocked_gps_access'));
      };

      navigator.geolocation.getCurrentPosition((position) => { setGps(position.coords.latitude, position.coords.longitude); }, () => { onUserDenied(); });
    } else {
      this.toast.toast(this.translate.instant('you_use_old_browser'));
    }
  }


  predefinedReset() {
    this.filter.city.full_address = '';
    this.filter.city.gpsLatitude = 0;
    this.filter.city.gpsLongitude = 0;
    this.filter.city.isActive = false;
    this.filter.options = [];
    this.filter.more = {
      isActive: false,
      crowd: {
        isActive: false,
          val: [0, 50]
      },
      gender: {
        isActive: false,
          val: 0
      },
      age: {
        isActive: false,
          val: [0, 100]
      }
    };
    this.filter.predefinedFilter = null;

    this.getEvents({});
  }


  loadMoreEvents() {
    this.filter.pageNb += 1;


    const body = {
      gpsLatitude: this.filter.city.gpsLatitude,
      gpsLongitude: this.filter.city.gpsLongitude,
      pageNb: this.filter.pageNb,
      // mapZoom: this.filter.mapZoom ? this.filter.mapZoom : null
    };



    this.api.getPB(`events`, body).safeSubscribe(this, (res: any) => {
      if (Object.getOwnPropertyNames(res.events).length > 0) {
        for (const event of res.events) {
          event.placeInfo = this.commonService.getInfoFromPlaceId(event.google_place_id);
        }

        this.events = this.events.concat(res.events);
      } else {
        --this.filter.pageNb;
      }
    });
  }


  newEventPop() {
    if (!this.O4PUser.loggedIn) {
      this.logInPop();
      return null;
    }


    this.createEventDialogRef = this.dialog.open(CreateEventPop);

    this.createEventDialogRef.afterClosed().safeSubscribe(this, res => {
      console.log(res);
      if (!!res) {

        if (res.file.arr[0].secure_id) {
          res.body.cover_picture = res.file.arr[0].secure_id;

          this.api.postPB(`events`, res.body).safeSubscribe(this, (res2: any) => {
            this.predefinedFilterFunc('reset');
            this.toast.toast(this.translate.instant('event_created'));
            this.router.navigate(['event', res2.event.secure_id]);
          });
        } else {
          const formData = new FormData();
          formData.append('file', res.file.arr[0].file);
          console.log(formData);

          this.api.uploadB(formData).safeSubscribe(this, (res2: any) => {
            res.body.cover_picture = res2.uploaded.secure_ids[0];

            this.api.postPB(`events`, res.body).safeSubscribe(this, (res2: any) => {
              this.predefinedFilterFunc('reset');
              this.toast.toast(this.translate.instant('event_created'));
              this.router.navigate(['event', res2.event.secure_id]);
            });
          });
        }
      }
    });
  }


  logInPop() {
    this.logInPopRef = this.dialog.open(LoginPop, <MdDialogConfig>{
      position: {top: '30px', right: '30px'}
    });
  }


  tourPop() {
    if (!localStorage.getItem('O4PTour')) {
      this.tourPopRef = this.dialog.open(TourPop, <MdDialogConfig>{});
      localStorage.setItem('O4PTour', JSON.stringify(true));
  
  
  
      this.tourPopRef.afterClosed().safeSubscribe(this, res => {
        this.intro();
      });
      
      return;
    }
  
  
    this.intro();
  }
  
  
  intro() {
    setTimeout(() => {
      if (!localStorage.getItem('O4PIntroHome') && innerWidth > 640) {
        localStorage.setItem('O4PIntroHome', JSON.stringify(true));
    
        this.commonService.introJs.setOptions({
          showStepNumbers: false,
          tooltipPosition: 'auto',
          steps: [
            {
              element: document.getElementById('home-intro-one'),
              intro: this.translate.instant('tourjs_2_1')
            },
            {
              // element: document.getElementById('home-intro-two'),
              intro: this.translate.instant('tourjs_2_2')
            },
            {
              element: document.getElementById('home-intro-three'),
              intro: this.translate.instant('tourjs_2_3')
            },
            {
              element: document.getElementById('home-intro-four'),
              intro: this.translate.instant('tourjs_2_4')
            },
            {
              // element: document.getElementById('home-intro-five'),
              intro: this.translate.instant('tourjs_2_5')
            },
            {
              element: document.getElementById('home-intro-six'),
              intro: this.translate.instant('tourjs_2_6')
            },
            {
              element: document.getElementById('home-intro-seven'),
              intro: this.translate.instant('tourjs_2_7')
            }
          ]
        });
    
        this.commonService.introJs.start();
      }
    }, 4000);
  }


  findPanBy() {
    // window.onresize = (event) => {
    // };

    const winw = window.innerWidth;
    const boxw = document.querySelector('.p-home-b').clientWidth;
    const filterw = 75;

    this.commonService.mapPanBy = ((winw - boxw) / 2) - filterw;
  }
}





