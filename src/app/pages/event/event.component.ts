import {Component, OnInit, AfterViewInit, HostBinding} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {MdDialog, MdDialogConfig, MdDialogRef} from '@angular/material';
import {Meta} from '@angular/platform-browser';
import {TranslateService} from '@ngx-translate/core';
import {Md2Toast} from 'md2';
import {Animations} from '../../animations/animations';
import {SafeSubscribe} from '../../helpers/safe-subscripe/safe-subscripe';
import {StatisticsPop, LiveDescPop, UpdateEventTopPop, UpdateEventBotPop, SendMessagePop, ParticipationPop, InviteMorePop, LoginPop, SeeAllPop} from '../../pop';
import {ApiService, StoreService, CommonService} from '../../services';


@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss'],
  animations: Animations.page
})
export class EventComponent extends SafeSubscribe implements OnInit, AfterViewInit {
  @HostBinding('@routeAnimation') routeAnimation = true;
  
  statisticsDialogRef: MdDialogRef<StatisticsPop>;
  liveDescPopRef: MdDialogRef<LiveDescPop>;
  editEventTopDialogRef: MdDialogRef<UpdateEventTopPop>;
  editEventBotDialogRef: MdDialogRef<UpdateEventBotPop>;
  sendMessagePopRef: MdDialogRef<SendMessagePop>;
  participationPopRef: MdDialogRef<ParticipationPop>;
  invitePopRef: MdDialogRef<InviteMorePop>;
  logInPopRef: MdDialogRef<LoginPop>;
  seeAllPopRef: MdDialogRef<SeeAllPop>;
  event: any;
  O4PUser: any;
  gender_balance: any;
  user = {
    account_type: 'premium',
  };
  comment = {
    val: '',
  };


  constructor(
    private api: ApiService,
    private store: StoreService,
    public dialog: MdDialog,
    private commonService: CommonService,
    private route: ActivatedRoute,
    private translate: TranslateService,
    private toast: Md2Toast,
    private meta: Meta
  ) {
    super();

    this.commonService.userAsync.safeSubscribe(this, (value) => {
      this.O4PUser = value;
    });
  }



  ngOnInit() {
    this.route.params.safeSubscribe(this, () => {
      this.getEvent();
    });
  }
  
  
  ngAfterViewInit() {
    if (!localStorage.getItem('O4PIntroEvent')) {
      localStorage.setItem('O4PIntroEvent', JSON.stringify(true));
  
      const interval = setInterval(() => {
        if (document.getElementById('event-intro-one')) {
          clearInterval(interval);
  
          this.commonService.introJs.setOptions({
            showStepNumbers: false,
            hintPosition: 'auto',
            steps: [
              {
                element: document.getElementById('event-intro-one'),
                intro: this.translate.instant('tourjs_1_1')
              },
              {
                element: document.getElementById('event-intro-two'),
                intro: this.translate.instant('tourjs_1_2')
              },
              {
                element: document.getElementById('event-intro-three'),
                intro: this.translate.instant('tourjs_1_3')
              },
              {
                element: document.getElementById('event-intro-four'),
                intro: this.translate.instant('tourjs_1_4')
              },
              {
                element: document.getElementById('event-intro-five'),
                intro: this.translate.instant('tourjs_1_5')
              },
              {
                element: document.getElementById('event-intro-six'),
                intro: this.translate.instant('tourjs_1_6')
              },
              {
                element: document.getElementById('event-intro-seven'),
                intro: this.translate.instant('tourjs_1_7')
              }
            ]
          });
  
          this.commonService.introJs.start();
        }
      }, 100);
    }
  }


  getEvent() {
    this.api.getEvent(this.route.snapshot.params['id']).safeSubscribe(this);


    this.store.changes.map(res => res.event).safeSubscribe(this, (res) => {
      if (!res || !res.event) { return null; }
      this.event = res.event;
      if (this.event.event_type === 'o4p') {
        this.event.o4p = 'o4p';
        this.event.event_type = 'public';
      } else {
        this.event.o4p = 'limitations';
      }


      /**
       * TRANSFORM GENDER
       * **/
      this.gender_balance = this.genderTransform(this.event.gender_balance);


      /**
       * GET INFO FROM PLACE ID
       * **/
      const city = this.commonService.getCityByPlaceId(this.event.google_place_id);

      if (!city) {
        this.commonService.getInfoFromPlaceId(this.event.google_place_id).safeSubscribe(this, (res: any) => {
          this.event.city_country = res.city_country;
          this.event.full_address = res.full_address;
          this.commonService.pushCity(res);
        });
      } else {
        this.event.city_country = city.city_country;
        this.event.full_address = city.full_address;
      }


      /**
       * SET COMMENT
       * **/
      this.comment = {
        val: res.event.can_comment ? '' : this.translate.instant('You need to be a confirmed participant to leave a comment here')
      };


      /**
       * SET META FOR FB
       * **/
      this.setEventMeta();
    });
  }


  onMedia(e) {
    this.api.uploadB(e.formData).safeSubscribe(this, (res: any) => {
      res.uploaded.secure_ids.forEach((item, i) => {

        const body = {
          media_secure_id: item,
          label: `${e.title} ${i > 0 ? i : ''}`
        };

        this.api.postPB(`events/${this.event.secure_id}/medias`, body).safeSubscribe(this, () => {
          if (i === res.uploaded.secure_ids.length - 1) {
            this.getEvent();
            this.toast.toast(this.translate.instant('media_uploaded'));
          }
        });
      });
    });
  }


  editEventTop() {
    this.editEventTopDialogRef = this.dialog.open(UpdateEventTopPop, <MdDialogConfig>{
      data: this.event
    });

    this.editEventTopDialogRef.afterClosed().safeSubscribe(this, res => {
      if (!!res) {


        // immediately update
        // this.event.title = res.body.title;
        // this.event.event_nature = res.body.event_nature;
        // this.event.start_date = res.body.start_date;
        // this.event.end_date = res.body.end_date;
        // this.event.google_place_id = res.body.google_place_id;
        // this.event.media_secure_id = res.body.media_secure_id;
        // immediately update



        if (res.file.arr[0].media_secure_id) {
          res.body.media_secure_id = res.file.arr[0].media_secure_id;

          this.api.putPB(`events/${this.event.secure_id}`, {fields: res.body}).safeSubscribe(this, () => {
            this.getEvent();
            this.toast.toast(this.translate.instant('event_updated'));
          });
        } else {
          const formData = new FormData();
          formData.append('file', res.file.arr[0].file);

          this.api.uploadB(formData).safeSubscribe(this, (res2: any) => {
            res.body.media_secure_id = res2.uploaded.secure_ids[0];

            this.api.putPB(`events/${this.event.secure_id}`, {fields: res.body}).safeSubscribe(this, () => {
              this.getEvent();
              this.toast.toast(this.translate.instant('event_updated'));
            });
          });
        }
      }
    });
  }


  editEventBot() {
    this.editEventBotDialogRef = this.dialog.open(UpdateEventBotPop, <MdDialogConfig>{
      data: this.event
    });

    this.editEventBotDialogRef.afterClosed().safeSubscribe(this, res => {
      if (!!res) {

        // this.event.condition = res.condition;
        // this.event.items = res.items;
        // this.event.price = res.price;
        // this.event.event_type = res.event_type;
        // this.event.nb_people_min = res.nb_people_min;
        // this.event.nb_people_max = res.nb_people_max;
        // this.event.gender_balance = res.gender_balance;
        // this.event.age_min = res.age_min;
        // this.event.age_max = res.age_max;
        // this.event.options = res.options;
        // this.event.description = res.description;

        this.api.putPB(`events/${this.event.secure_id}`, {fields: res}).safeSubscribe(this, () => {
          this.getEvent();
          this.toast.toast(this.translate.instant('event_updated'));
        });
      }
    });
  }


  genderTransform(value) {
    let out = value;

    switch (value) {
      case 0: out = this.translate.instant('only_men'); break;
      case 1: out = this.translate.instant('more_men'); break;
      case 2: out = this.translate.instant('balanced'); break;
      case 3: out = this.translate.instant('more_women'); break;
      case 4: out = this.translate.instant('only_women'); break;
    }

    return out;
  }


  // livePop() {
  //   this.livePopRef = this.dialog.open(LivePop, {
  //     width: '600px',
  //     height: '400px'
  //   });
  // }


  liveDescPop() {
    this.liveDescPopRef = this.dialog.open(LiveDescPop, <MdDialogConfig>{
      width: '300px',
      data: this.event
    });


    this.liveDescPopRef.afterClosed().safeSubscribe(this, res => {
      if (!!res) {
        this.getEvent();
      }
    });
  }


  sendMessagePop() {
    if (!this.O4PUser.loggedIn) {
      this.logInPopRef = this.dialog.open(LoginPop, <MdDialogConfig>{
        position: {top: '30px', right: '30px'}
      });
      return null;
    }


    this.sendMessagePopRef = this.dialog.open(SendMessagePop, <MdDialogConfig>{
      data: {
        data: this.event,
        type: 'to'
      }
    });
  }


  participationPop() {
    if (!this.O4PUser.loggedIn) {
      this.logInPopRef = this.dialog.open(LoginPop, <MdDialogConfig>{
        position: {top: '30px', right: '30px'}
      });
      return null;
    }

    this.participationPopRef = this.dialog.open(ParticipationPop, <MdDialogConfig>{
      data: {status: 'temp', event: this.event}
    });


    this.participationPopRef.afterClosed().safeSubscribe(this, res => {
      if (!!res) {
        this.getEvent();
      }
    });
  }


  seeAllPop() {
    this.seeAllPopRef = this.dialog.open(SeeAllPop, <MdDialogConfig>{
      data: this.event
    });


    this.seeAllPopRef.afterClosed().safeSubscribe(this, res => {
      if (!!res) {
        this.getEvent();
      }
    });
  }


  invitePop() {
    this.invitePopRef = this.dialog.open(InviteMorePop, <MdDialogConfig>{
      width: '600px',
      data: {secure_id: this.event.secure_id}
    });
  }


  setEventMeta() {
    this.meta.addTag({ property: 'og:url', content: `${window.location.origin}/event/${this.event.secure_id}` });
    this.meta.addTag({ property: 'og:type', content: 'article' });
    this.meta.addTag({ property: 'og:title', content: this.event.title });
    this.meta.addTag({ property: 'og:description', content: this.event.description });
    this.meta.addTag({ property: 'og:image', content: this.event.image });
  }








  participationCard() {
    this.api.getParticipationCard(`events/${this.event.secure_id}/participation_card`).safeSubscribe(this, (res: any) => {
      const w = window.open();
      w.document.write(res._body);
      w.document.close(); // necessary for IE >= 10
      w.focus(); // necessary for IE >= 10
      return true;
    });
  }


  attendeeList() {
    this.api.getAttendeeList(`events/${this.event.secure_id}/attendee_list`).safeSubscribe(this, (res: any) => {
      if (res.ok) {
        const w = window.open();
        w.document.write(res._body);
        w.document.close(); // necessary for IE >= 10
        w.focus(); // necessary for IE >= 10
        return true;
      }
    });
  }


  getStatistics() {

    this.statisticsDialogRef = this.dialog.open(StatisticsPop, <MdDialogConfig>{
      data: this.event,
      width: '600px'
    });

    this.statisticsDialogRef.afterClosed().safeSubscribe(this, res => {
      if (!!res) {
      }
    });
  }
}

