import {Component, OnInit, AfterViewInit, HostBinding} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {ActivatedRoute} from '@angular/router';
import {MdDialog, MdDialogConfig, MdDialogRef} from '@angular/material';
import {FacebookService, LoginResponse} from 'ngx-facebook';
import {Md2Toast} from 'md2';
import {Animations} from '../../animations/animations';
import {SafeSubscribe} from '../../helpers/safe-subscripe/safe-subscripe';
import {UpdateUserTopPop, UpdateUserBotPop, ConfirmPop} from '../../pop';
import {ApiService, StoreService, CommonService} from '../../services';


@Component({
  selector: 'app-profile',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
  animations: Animations.page
})
export class ProfileComponent extends SafeSubscribe implements OnInit, AfterViewInit {
  @HostBinding('@routeAnimation') routeAnimation = true;
  
  editUserTopDialogRef: MdDialogRef<UpdateUserTopPop>;
  editUserBotDialogRef: MdDialogRef<UpdateUserBotPop>;
  deleteConfirmDialogRef: MdDialogRef<ConfirmPop>;
  O4PUser: any;
  user: any;
  comment = {
    val: ''
  };
  userId: any;
  languages: any;

  constructor(
    private route: ActivatedRoute,
    private commonService: CommonService,
    public dialog: MdDialog,
    private api: ApiService,
    private store: StoreService,
    private translate: TranslateService,
    private fb: FacebookService,
    private toast: Md2Toast
  ) {
    super();
    // socket.on('refreshUser', function (data) {
    //   if(this.route.snapshot.params['id'] === data.secure_id){
    //     this.getUser();
    //   }
    // });



    this.commonService.userAsync.safeSubscribe(this, (value) => {
      this.O4PUser = value;
    });
  
    this.commonService.languagesAsync.safeSubscribe(this, (value) => {
      this.languages = value;
    });
  }



  ngOnInit() {
    this.route.params.safeSubscribe(this, params => {
      this.userId = params['id'];
      this.getUser();
    });
  }
  
  
  ngAfterViewInit() {
    if (!localStorage.getItem('O4PIntroUser')) {
      localStorage.setItem('O4PIntroUser', JSON.stringify(true));
  
  
      const interval = setInterval(() => {
        if (document.getElementById('user-intro-one')) {
          clearInterval(interval);
      
          this.commonService.introJs.setOptions({
            showStepNumbers: false,
            hintPosition: 'auto',
            steps: [
              {
                element: document.getElementById('user-intro-one'),
                intro: this.translate.instant('tourjs_3_1')
              },
              {
                element: document.getElementById('user-intro-two'),
                intro: this.translate.instant('tourjs_3_2')
              },
              {
                element: document.getElementById('user-intro-three'),
                intro: this.translate.instant('tourjs_3_3')
              },
              {
                element: document.getElementById('user-intro-four'),
                intro: this.translate.instant('tourjs_3_4')
              },
              {
                element: document.getElementById('user-intro-five'),
                intro: this.translate.instant('tourjs_3_5')
              },
              {
                element: document.getElementById('user-intro-six'),
                intro: this.translate.instant('tourjs_3_6')
              }
            ]
          });
      
          this.commonService.introJs.start();
        }
      }, 100);
    }
  }
  

  getUser() {
    this.api.getUser(this.userId).safeSubscribe(this);


    this.store.changes.map(res => res.user).safeSubscribe(this, (res) => {
      if (!res) { return null; }

      this.user = res.user;


      /**
       * GET INFO FROM PLACE ID
       * **/
      if (this.user.google_place_id) {
        const city = this.commonService.getCityByPlaceId(this.user.google_place_id);

        if (!city) {
          this.commonService.getInfoFromPlaceId(this.user.google_place_id).safeSubscribe(this, (placeres: any) => {
            this.user.country = placeres.country;
            this.user.full_address = placeres.full_address;
            this.commonService.pushCity(placeres);
          });
        } else {
          this.user.country = city.country;
          this.user.full_address = city.full_address;
        }
      }


      /**
       * SET COMMENT
       * **/
      this.comment = {
        val: this.user.can_comment ? '' : this.translate.instant('you_need_to_have_common_participations_with_this_user_to_leave_a_comment')
      };


      /**
       * CHECK AGE
       * **/
      if (!isNaN(this.user.birthdate)) {
        this.user.age = this.calculateAge(new Date(this.user.birthdate));
      } else {

      }


      /**
       * IF NOT GLOBAL USER PAGE, CHECK ACCESS
       * **/
      this.user.can_see_address = this.O4PUser.user.secure_id === this.user.secure_id ? true : this.user.is_address_public;
      this.user.can_see_phone = this.O4PUser.user.secure_id === this.user.secure_id ? true : this.user.is_phone_public;
      this.user.can_see_email = this.O4PUser.user.secure_id === this.user.secure_id ? true : this.user.is_email_public;
      this.user.can_see_birthday = this.O4PUser.user.secure_id === this.user.secure_id ? true : this.user.is_birthday_public;


      /**
       * UPDATE USER IF HIS PAGE
       * **/
      if (this.O4PUser.user.secure_id === this.user.secure_id) {
        this.O4PUser.user = this.user;
        this.commonService.setUser(this.O4PUser);
      }
  
  
  
      this.setUserLang();
    });


    this.api.getPB(`notifications`, {page_nb: 0}).safeSubscribe(this, (res: any) => {
      const a = this.commonService.O4PUser.getValue();
      a.notifications = res.notifications;
      a.total_notifications = 0;
      if (Object.getOwnPropertyNames(a.notifications).length > 0) {
        a.notifications.forEach((item) => {
          if (item.notif.status !== 'displayed') {
            a.total_notifications++;
          }
        });
      }
      this.commonService.setUser(a);
    });
  }


  editUserTopPop() {
    this.editUserTopDialogRef = this.dialog.open(UpdateUserTopPop, <MdDialogConfig>{
      data: this.user
    });

    this.editUserTopDialogRef.afterClosed().safeSubscribe(this, res => {
      // console.log(res);
      if (!!res) {

        // this.user.description = res.description;
        // this.user.username = res.username;

        if (res.file.arr[0].media_secure_id) {
          res.body.media_secure_id = res.file.arr[0].media_secure_id;

          this.api.putPB(`users/${this.user.secure_id}`, {fields: res.body}).safeSubscribe(this, () => {
            this.getUser();
            this.toast.toast(this.translate.instant('user_updated'));
          });
        } else {
          const formData = new FormData();
          formData.append('file', res.file.arr[0].file);

          this.api.uploadB(formData).safeSubscribe(this, (res2: any) => {
            res.body.media_secure_id = res2.uploaded.secure_ids[0];

            this.api.putPB(`users/${this.user.secure_id}`, {fields: res.body}).safeSubscribe(this, () => {
              this.getUser();
              this.toast.toast(this.translate.instant('user_updated'));
            });
          });
        }
      }
    });
  }


  editUserBotPop() {
    this.editUserBotDialogRef = this.dialog.open(UpdateUserBotPop, <MdDialogConfig>{
      data: this.user
    });

    this.editUserBotDialogRef.afterClosed().safeSubscribe(this, res => {
      // console.log(res);
      if (!!res) {

        // this.user.is_nationality_public = res.is_nationality_public;
        // this.user.placeInfo = res.country;
        // this.user.is_address_public = res.is_address_public;
        // this.user.address = res.address;
        // this.user.is_phone_public = res.is_phone_public;
        // this.user.phone = res.phone;
        // this.user.is_email_public = res.is_email_public;
        // this.user.email = res.email;
        // this.user.is_birthday_public = res.is_birthday_public;
        // this.user.gender = res.gender;
        // this.user.email_notifications = res.email_notifications;
        // this.user.newsletter = res.newsletter;
        // this.user.email_events = res.email_events;
        // this.user.birthdate = res.birthdate;
        // this.user.language_id = res.language_id;
        // this.user.nationality_google_place_id = res.nationality_google_place_id;
        // this.user.google_place_id = res.google_place_id;
        // this.user.age = this.calculateAge(new Date(res.birthdate));

        this.api.putPB(`users/${this.user.secure_id}`, {fields: res}).safeSubscribe(this, (userres: any) => {
          if (userres._body) {
            const body = JSON.parse(userres._body);
            if (body.statusText && body.statusText !== 'OK') {
              this.toast.toast(body.statusText);
              return;
            }
          }

          this.getUser();

          if (userres.status !== 403) {
            this.toast.toast(this.translate.instant('user_updated'));
          }
        });
      }
    });
  }


  onMedia(e) {
    this.api.uploadB(e.formData).safeSubscribe(this, (res: any) => {
      res.uploaded.secure_ids.forEach((item, i) => {

        const body = {
          media_secure_id: item,
          label: `${e.title} ${i > 0 ? i : ''}`
        };

        this.api.postPB(`users/${this.user.secure_id}/medias`, body).safeSubscribe(this, () => {
          if (i === res.uploaded.secure_ids.length - 1) {
            this.getUser();
            this.toast.toast(this.translate.instant('media_uploaded'));
          }
        });
      });
    });
  }


  slideArr(arr, dir) {
    switch (dir) {
      case 0:
        arr.unshift(arr.pop());
        break;
      case 1:
        arr.push(arr.shift());
        break;
    }
  }


  calculateAge(birthday) {
    const ageDifMs = Date.now() - birthday.getTime();
    const ageDate = new Date(ageDifMs);
    return Math.abs(ageDate.getUTCFullYear() - 1970);
  }


  linkToFb() {
    this.fb.login().then((loginres: LoginResponse) => {
      this.fb.api('/me?fields=name,email,age_range,picture,location').then((meres: any) => {
      
        const body = {
          fb_user_id: loginres.authResponse.userID,
          fb_access_token: loginres.authResponse.accessToken
        };
  
        this.api.postPB(`users/${this.user.secure_id}/fb_link`, body).safeSubscribe(this, (res) => {
          console.log(res);
          this.getUser();
        });
      }).catch(() => {
        this.toast.toast(this.translate.instant('cant_get_data_from_facebook'));
      });
    }).catch(() => {
      this.toast.toast(this.translate.instant('logged_in_error'));
    });
  }


  unlinkFromFb() {
    // this.fb.api('/me?fields=name,email,age_range,picture,location').then((res: any) => {});

    this.deleteConfirmDialogRef = this.dialog.open(ConfirmPop, <MdDialogConfig>{
      width: '500px',
      data: {
        title: this.translate.instant('confirm_unlink_from_Facebook')
      }
    });

    this.deleteConfirmDialogRef.afterClosed().safeSubscribe(this, res => {
      if (!!res) {
        this.api.deleteP(`users/${this.user.secure_id}/fb_link`).safeSubscribe(this, (res) => {
          console.log(res);
          this.getUser();
        });
      }
    });
  }
  
  
  addParticipation(participation_secure_id, status) {
    const body = {
      secure_id: this.user.secure_id,
      status: status
    };
    
    this.api.putPB(`participations/${participation_secure_id}`, body).safeSubscribe(this, (res: any) => {
      this.getUser();
      
      if (res.statusText) {
        this.toast.toast(res.statusText);
      }
    });
  }
  
  
  disableBtn(e) {
    e.target.disabled = true;
  };
  

  setUserLang() {
    let current: any;
    this.languages.arr.forEach(item => {
      if (item.id === this.O4PUser.user.language_id) {
        current = item;
        console.log(item);
      }
    });
  
    current = current || this.languages.arr[0];

    this.commonService.setLanguages({
      current: current,
      arr: this.languages.arr
    });
  }
}




