import {Directive, ElementRef} from '@angular/core';

@Directive({
  selector: '[appScrollx]'
})
export class ScrollxDirective {
  constructor(private el: ElementRef) {
    el.nativeElement.addEventListener('mousewheel', (e) => {
      el.nativeElement.scrollLeft = el.nativeElement.scrollLeft + e.deltaY;
      e.preventDefault();
      return false;
    });
  }
}


