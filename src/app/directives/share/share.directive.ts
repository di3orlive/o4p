import {Directive, HostListener, Input} from '@angular/core';
import {FacebookService} from 'ngx-facebook';

@Directive({
  selector: '[appShare]'
})
export class ShareDirective {
  @Input() share: any;

  constructor (private fb: FacebookService) {}

  @HostListener('click', ['$event'])
  onClick(e) {
    e.preventDefault();
    e.stopPropagation();


    if (this.share.type === 'tw') {
      window.open(`http://twitter.com/share?text=Open4Partiz ${window.location.origin}/event/${this.share.event.secure_id}&hashtags=open4partiz,o4p`, '_blank');
    } else if (this.share.type === 'fb') {
      this.fb.ui({
        method: 'feed',
        link: this.share.event.seo_share_url
      });
    }
  }
}
