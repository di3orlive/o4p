import {Directive, ElementRef, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {Md2Toast} from 'md2';

declare const paypal;

@Directive({
  selector: '[appPpcobtn]'
})
export class PaypalDirective implements OnInit {
  @Input() appPpcobtn: any;
  @Output() onCharge = new EventEmitter();

  constructor(
    private el: ElementRef,
    private toast: Md2Toast,
    private translate: TranslateService
  ) {
  }


  ngOnInit() {
    if (this.appPpcobtn) {
      paypal.Button.render({
        env: this.appPpcobtn.parameters.paypal_mode,
        client: {
          sandbox: this.appPpcobtn.parameters.paypal_sandbox_key,
          production: this.appPpcobtn.parameters.paypal_production_key,
        },
        commit: true,
        payment: (data, actions) => {
          return actions.payment.create({
            payment: {
              transactions: [
                {
                  amount: { total: this.appPpcobtn.amount, currency: 'EUR'},
                  custom : this.appPpcobtn.custom
                }
              ]
            }
          });
        },
        onError: (err) => {
          this.toast.toast(this.translate.instant('error'));
        },
        onAuthorize: (data, actions) => {
          return actions.payment.execute().then((payment) => {
            console.log(data);
            // The payment is complete!
            // You can now show a confirmation message and send the the data to the server, I think you can return the whole "data" object
        
            this.toast.toast(this.translate.instant('the_payment_is_complete'));
            this.onCharge.emit(payment);
          });
        }
      }, this.el.nativeElement);
    }
  }
}
