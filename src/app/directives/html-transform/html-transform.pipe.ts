import {AfterViewInit, Directive, ElementRef, Input, OnDestroy, Renderer2} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {Router} from '@angular/router';
import {MdDialog} from '@angular/material';
import {SafeSubscribe} from "../../helpers/safe-subscripe/safe-subscripe";

@Directive({
  selector: '[htmlTransform]'
})
export class HtmlTransformDirective extends SafeSubscribe implements AfterViewInit, OnDestroy {
  @Input() htmlTransform: string;
  listenClickFunc: any;
  
  constructor(private element: ElementRef,
              private translate: TranslateService,
              private router: Router,
              private dialog: MdDialog,
              private renderer: Renderer2) {
    super();
  }
  
  ngAfterViewInit() {
    // console.log(this.htmlTransform);
  
    // this.element.nativeElement.innerHTML = this.htmlTransform;
    
    this.translate.get(this.htmlTransform).safeSubscribe(this, (translateText: string) => {
      this.element.nativeElement.innerHTML = translateText;
// this.cdRef.detectChanges();

      const navigationElements = Array.prototype.slice.call(this.element.nativeElement.querySelectorAll('[routerLink]'));
      navigationElements.forEach(elem => {
        this.renderer.listen(elem, 'click', (event) => {
          event.preventDefault();
  
          this.dialog.closeAll();
          this.router.navigate([elem.getAttribute('routerLink')]);
        });
      });
    });
  }
  
  ngOnDestroy() {
    // this.dialog.closeAll();
    // this.listenClickFunc();
  }
}

