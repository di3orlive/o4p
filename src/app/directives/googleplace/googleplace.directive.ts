import {Directive, ElementRef, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SafeSubscribe} from '../../helpers/safe-subscripe/safe-subscripe';
import {CommonService} from '../../services';
declare const google;


@Directive({
  selector: '[appGoogleplace]'
})
export class GoogleplaceDirective extends SafeSubscribe implements OnInit {
  @Input() appGoogleplace: any;
  @Input() countriesOnly: any;
  @Output() place = new EventEmitter();
  options = {
    types: '' ? ['geocode'] : [],
    componentRestrictions: {}
  };
  gPlace: any;


  constructor(
    private el: ElementRef,
    private commonService: CommonService
  ) {
    super();

  }


  ngOnInit () {
    this.checkPlace();
  }


  checkPlace() {
    if (this.appGoogleplace) {
      const city = this.commonService.getCityByPlaceId(this.appGoogleplace);
      const body = {
        place_id: '',
        city_country: ''
      };

      if (!city) {
        this.commonService.getInfoFromPlaceId(this.appGoogleplace).safeSubscribe(this, (placeres: any) => {
          body.place_id = this.appGoogleplace;
          body.city_country = placeres.city_country;

          this.commonService.pushCity(placeres);
        });
      } else {
        body.place_id = this.appGoogleplace;
        body.city_country = city.city_country;
      }

      this.place.emit(body);
    }



    if (this.countriesOnly) {
      this.options = {
        types: ['(cities)'],
        componentRestrictions: {country: 'us'}
      };
    }




    this.gPlace = new google.maps.places.Autocomplete(this.el.nativeElement, this.options);
    this.gPlace.setComponentRestrictions({'country': []});


    google.maps.event.addListener(this.gPlace, 'place_changed', () => {
      const place = this.gPlace.getPlace();


      if (place.place_id) {
        const placeInfo = this.commonService.placeIdCityFilter(place.address_components);

        const body = {
          place_id: place.place_id,
          city_country: placeInfo.city_country,
          city: placeInfo.city,
          country: placeInfo.country,
          full_address: place.formatted_address,
          gpsLatitude: place.geometry.location.lat(),
          gpsLongitude: place.geometry.location.lng()
        };

        this.place.emit(body);
      }
    });
  }
}


