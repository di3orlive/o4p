import {Component} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {FacebookService} from 'ngx-facebook';
import {SafeSubscribe} from './helpers/safe-subscripe/safe-subscripe';
import {ApiService, SocketService, CommonService} from './services';
import {TranslateService} from "@ngx-translate/core";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent extends SafeSubscribe {
  isHomePage: any;
  progress = 0;
  options: any;

  constructor(
    private router: Router,
    private translate: TranslateService,
    private commonService: CommonService,
    private api: ApiService,
    private socketService: SocketService,
    private fb: FacebookService
  ) {
    super();
    
    
    fb.init({
      appId: '679107545579173',
      xfbml: true,
      version: 'v2.9'
    });

    this.router.events.safeSubscribe(this, (event) => {
      if (event instanceof NavigationEnd) {
        this.isHomePage = event.url === '/';
      }
    });


    this.commonService.progressAsync.safeSubscribe(this, (value) => {
      this.progress = value;
    });


    this.api.getP(`languages`).subscribe((resp: any) => {
      let current: any;
      resp.languages.forEach(item => {
        if (item.id === JSON.parse(localStorage.getItem('O4PLang'))) {
          current = item;
          console.log(item);
        }
      });
  
      current = current || resp.languages[0];
      
      this.commonService.setLanguages({
        current: current,
        arr: resp.languages
      });
    });
  
  
  
    this.api.getP(`parameters`).subscribe((res: any) => {
      this.commonService.setParameters(res);
    });
  
  
  
    this.api.getNotifications();


    this.socketService.init();
    this.socketService.onNoti().safeSubscribe(this, () => {
      this.api.getNotifications();
    });
  }
}

