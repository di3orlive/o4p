import {Injectable, ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule, CanActivate, Router, RouterStateSnapshot, ActivatedRouteSnapshot} from '@angular/router';
import {MdDialog, MdDialogConfig, MdDialogRef} from '@angular/material';
import {HomeComponent, EventComponent, ProfileComponent} from './pages';
import {CommonService} from './services/common/common.service';
import {LoginPop} from './pop/login/login.pop';


@Injectable()
export class AuthGuard implements CanActivate {
  logInPopRef: MdDialogRef<LoginPop>;
  O4PUser: any;

  constructor(
    private router: Router,
    public dialog: MdDialog,
    private commonService: CommonService,
  ) {
    this.commonService.userAsync.subscribe((value) => {
      this.O4PUser = value;
    });
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this.O4PUser.loggedIn) {
      return true;
    } else {
      this.router.navigate(['']);

      this.logInPopRef = this.dialog.open(LoginPop, <MdDialogConfig>{
        position: {top: '30px', right: '30px'}
      });

      return false;
    }
  }
}


export const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'event/:id', component: EventComponent },
  { path: 'user/:id', component: ProfileComponent, canActivate: [AuthGuard] },
  { path: '**', component: HomeComponent}
];


export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
