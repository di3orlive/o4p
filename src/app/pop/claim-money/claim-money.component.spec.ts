import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClaimMoneyPop } from './claim-money.component';

describe('ClaimMoneyPop', () => {
  let component: ClaimMoneyPop;
  let fixture: ComponentFixture<ClaimMoneyPop>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClaimMoneyPop ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClaimMoneyPop);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
