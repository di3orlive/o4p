import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MdDialogRef} from '@angular/material';
import {CommonService} from '../../services';
import {SafeSubscribe} from "../../helpers/safe-subscripe/safe-subscripe";

@Component({
  selector: 'app-claim-money',
  templateUrl: './claim-money.component.html',
  styleUrls: ['./claim-money.component.scss']
})
export class ClaimMoneyPop extends SafeSubscribe implements OnInit {
  form: FormGroup;
  O4PUser: any;
  ppco: any;
  
  constructor(
    public dialogRef: MdDialogRef<ClaimMoneyPop>,
    private commonService: CommonService
  ) {
    super();
    this.commonService.userAsync.safeSubscribe(this, (value) => {
      this.O4PUser = value;
    });
    
    this.form = new FormGroup({
      num: new FormControl(null, Validators.required)
    });
  }
  
  ngOnInit() {
  }
  
  checkNum() {
    this.ppco = {total: this.form.controls.num.value, custom: this.O4PUser.user.secure_id};
  }
  
  claim() {
    if (this.form.invalid) { return null; }
    
    this.dialogRef.close(this.form.controls.num.value);
  }
  
}
