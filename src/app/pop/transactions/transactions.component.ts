import {Component} from '@angular/core';
import {Sort} from '@angular/material';
import {SafeSubscribe} from '../../helpers/safe-subscripe/safe-subscripe';
import {ApiService} from '../../services/api/api.service';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss']
})
export class TransactionsComponent extends SafeSubscribe {
  transactions: any;
  
  sortedData;
  
  constructor(
    private api: ApiService
  ) {
    super();
    this.api.getP(`transactions`).safeSubscribe(this, (res: any) => {
      console.log(res.transactions);
  
      if (Object.keys(res.transactions).length > 0) {
        this.transactions = res.transactions;
        this.sortedData = res.transactions.slice();
      }
    });
  }
  
  sortData(sort: Sort) {
    const data = this.transactions.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }
    
    this.sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'date': return compare(a.name, b.name, isAsc);
        case 'label': return compare(+a.calories, +b.calories, isAsc);
        case 'amount': return compare(+a.fat, +b.fat, isAsc);
        case 'direction': return compare(+a.carbs, +b.carbs, isAsc);
        case 'status': return compare(+a.protein, +b.protein, isAsc);
        default: return 0;
      }
    });
  }
}

function compare(a, b, isAsc) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
