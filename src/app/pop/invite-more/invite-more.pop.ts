import {Component, Inject, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {MD_DIALOG_DATA, MdDialogRef} from '@angular/material';
import {Md2Toast} from 'md2';
import {SafeSubscribe} from '../../helpers/safe-subscripe/safe-subscripe';
import {ApiService} from '../../services';

@Component({
  selector: 'app-invite-more',
  templateUrl: './invite-more.pop.html',
  styleUrls: ['./invite-more.pop.scss']
})
export class InviteMorePop extends SafeSubscribe implements OnInit {
  users = [];
  email = '';


  constructor(
    public dialogRef: MdDialogRef<InviteMorePop>,
    @Inject(MD_DIALOG_DATA) public data: any,
    private translate: TranslateService,
    private toast: Md2Toast,
    private api: ApiService,
  ) {
    super();
  }

  ngOnInit() {
  }


  sendInvitations() {
    const usersArr = [];

    if (this.users[0]) {
      for (let i = 0; i < this.users.length; i++) {
        usersArr.push(this.users[i].username);
      }

      const body = {
        invitees: usersArr
      };

      this.api.postPB(`events/${this.data.secure_id}/invitations/`, body).safeSubscribe(this, () => {
        this.toast.toast(this.translate.instant('invitation_sent'));
        this.dialogRef.close();
      });
    } else {
      this.toast.toast(this.translate.instant('you_need_invite_at_least_one_friend'));
    }
  }



  onUser(e) {
    this.users = e;
  }
}

