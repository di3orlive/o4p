import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InviteMorePop } from './invite-more.pop';

describe('InviteMorePop', () => {
  let component: InviteMorePop;
  let fixture: ComponentFixture<InviteMorePop>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InviteMorePop ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InviteMorePop);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
