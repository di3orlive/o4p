import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatisticsPop } from './statistics.pop';

describe('StatisticsPop', () => {
  let component: StatisticsPop;
  let fixture: ComponentFixture<StatisticsPop>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatisticsPop ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatisticsPop);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
