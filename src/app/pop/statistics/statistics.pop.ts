import {Component, Inject, OnInit} from '@angular/core';
import {MD_DIALOG_DATA} from '@angular/material';
import {TranslateService} from '@ngx-translate/core';
import {SafeSubscribe} from '../../helpers/safe-subscripe/safe-subscripe';
import {ApiService} from '../../services/api/api.service';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.pop.html',
  styleUrls: ['./statistics.pop.scss']
})
export class StatisticsPop extends SafeSubscribe implements OnInit {
  statistics: any;
  lineChartData = [
    {data: [], label: `${this.translate.instant('price')} (€)`},
    {data: [], label: `${this.translate.instant('nb_of_participants')}`}
  ];
  lineChartLabels = [];
  lineChartOptions = {responsive: true};
  lineChartColors = [
    {backgroundColor: 'rgba(255, 22, 68, 0.2)', borderColor: '#ff1644', pointBackgroundColor: '#ff1644', pointBorderColor: '#fff', pointHoverBackgroundColor: '#fff', pointHoverBorderColor: '#ff1644'},
    {backgroundColor: 'rgba(0, 219, 184, 0.2)', borderColor: '#00dbb8', pointBackgroundColor: '#00dbb8', pointBorderColor: '#fff', pointHoverBackgroundColor: '#fff', pointHoverBorderColor: '#00dbb8'}
  ];
  lineChartLegend = true;
  lineChartType = 'line';

  constructor(
    private api: ApiService,
    private translate: TranslateService,
    @Inject(MD_DIALOG_DATA) public event: any,
  ) {
    super();
  }

  ngOnInit() {
    this.api.getP(`events/${this.event.secure_id}/statistics`).safeSubscribe(this, (res: any) => {
      this.statistics = res.statistics;

      this.statistics.participations.forEach((item) => {
        this.lineChartLabels.push(item.to_char);
        this.lineChartData[0].data.push(item.total_paid);
        this.lineChartData[1].data.push(item.count);
      });
    });
  }
}
