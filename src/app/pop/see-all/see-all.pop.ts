import {Component, Inject, OnInit} from '@angular/core';
import {MD_DIALOG_DATA, MdDialogRef} from '@angular/material';
import {SafeSubscribe} from '../../helpers/safe-subscripe/safe-subscripe';
import {ApiService, StoreService} from '../../services';

@Component({
  selector: 'app-see-all',
  templateUrl: './see-all.pop.html',
  styleUrls: ['./see-all.pop.scss']
})
export class SeeAllPop extends SafeSubscribe implements OnInit {

  constructor(
    public dialogRef: MdDialogRef<SeeAllPop>,
    @Inject(MD_DIALOG_DATA) public event: any,
    private api: ApiService,
    private store: StoreService,
  ) {
    super();
  }


  ngOnInit() {
  }


  addParticipation(participation_secure_id, status) {
    const body = {
      secure_id: this.event.secure_id,
      status: status
    };

    this.api.putPB(`participations/${participation_secure_id}`, body).safeSubscribe(this, () => {

      this.api.getEvent(this.event.secure_id).safeSubscribe(this);

      this.store.changes.map(res => res.events).safeSubscribe(this, (res) => {
        this.event = res.event;
      });
    });
  }


  disableBtn(e) {
    e.target.disabled = true;
  };
}
