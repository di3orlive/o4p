import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeeAllPop } from './see-all.pop';

describe('SeeAllPop', () => {
  let component: SeeAllPop;
  let fixture: ComponentFixture<SeeAllPop>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeeAllPop ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeeAllPop);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
