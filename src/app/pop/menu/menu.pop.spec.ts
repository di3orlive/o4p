import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuPop } from './menu.pop';

describe('MenuPop', () => {
  let component: MenuPop;
  let fixture: ComponentFixture<MenuPop>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuPop ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuPop);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
