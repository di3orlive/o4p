import { Component, OnInit, Input } from '@angular/core';
import {MdDialog, MdDialogRef, MdDialogConfig} from '@angular/material';
import {Router} from '@angular/router';
import {FacebookService, LoginResponse, LoginOptions} from 'ngx-facebook';
import {Md2Toast} from 'md2';
import {TranslateService} from '@ngx-translate/core';
import {LoginPop} from '../login/login.pop';
import {CreditsPop} from '../credits/credits.pop';
import {TermsPop} from '../terms/terms.pop';
import {FeedbackPop} from '../feedback/feedback.pop';
import {CreateAccPop} from '../create-acc/create-acc.pop';
import {CommonService, ApiService} from '../../services';
import {SafeSubscribe} from '../../helpers/safe-subscripe/safe-subscripe';


@Component({
  selector: 'app-menu-pop',
  templateUrl: './menu.pop.html',
  styleUrls: ['./menu.pop.scss']
})
export class MenuPop extends SafeSubscribe implements OnInit {
  @Input() sidenav: any;
  logInPopRef: MdDialogRef<LoginPop>;
  creditsPopRef: MdDialogRef<CreditsPop>;
  termsPopRef: MdDialogRef<TermsPop>;
  feedbackPopRef: MdDialogRef<FeedbackPop>;
  createAccPopRef: MdDialogRef<CreateAccPop>;
  O4PUser: any;
  languages: any;


  constructor(
    public dialog: MdDialog,
    private fb: FacebookService,
    private translate: TranslateService,
    private commonService: CommonService,
    private toast: Md2Toast,
    private router: Router,
    public api: ApiService
  ) {
    super();

    this.commonService.userAsync.safeSubscribe(this, (value) => {
      this.O4PUser = value;
  
  
      // if (this.O4PUser.curLang && this.languages) {
      //   this.languages.arr.forEach((item) => {
      //     if (item.id === this.O4PUser.curLang) {
      //       this.languages.current = item;
      //     }
      //   });
      //
      //   this.commonService.setLanguages(this.languages);
      // }
    });
  
    this.commonService.languagesAsync.safeSubscribe(this, (value) => {
      this.languages = value;
    });
  }
  
  
  ngOnInit() {
  }


  closeMenuPop() {
    this.sidenav.close();
  }
  
  
  FBLogIn({}) {
  this.api.O4PUser.showLoadingFb = true;
  const options: LoginOptions = {
  scope: 'email,public_profile,user_about_me,user_friends,user_likes,user_location,user_photos',
  return_scopes: true,
  enable_profile_selector: true
};
    this.fb.login(options).then((loginres: LoginResponse) => {
    
      console.log('Logged in', loginres);
      this.fb.api('/me?fields=name,email,age_range,picture,location').then((meres: any) => {
        this.toast.toast(this.translate.instant('logged_in'));
        
        const body = {
          fb_user_id: loginres.authResponse.userID,
          fb_access_token: loginres.authResponse.accessToken,
          sessionType: 'fbSession'
        };
        
        
        this.api.postPB(`sessions`, body).safeSubscribe(this, (res: any) => {
          console.log(res);
          
          const a = this.commonService.O4PUser.getValue();
          a.loggedIn = true;
          a.age = meres.age_range.min;
          a.email = meres.email;
          a.name = meres.name;
          a.image = meres.picture.data.url;
          a.sessionToken = res.session.token;
          a.sessionType = body.sessionType;
          this.commonService.setUser(a);
  this.api.O4PUser.showLoadingFb = false;
          this.getMe();
        });
      }).catch((e) => {
        this.toast.toast(this.translate.instant('cant_get_data_from_facebook'));
        console.error(e);
      });
    }).catch(e => {
      this.toast.toast(this.translate.instant('logged_in_error'));
      console.error(e);
    });
    
  }
  
  
  getMe() {
    const a = this.commonService.O4PUser.getValue();
    
    this.api.getP(`users/me`).safeSubscribe(this, (me: any) => {
      a.loggedIn = true;
      a.user = me.user;
      a.image = me.user.image;
      
      
      this.api.getPB(`notifications`, {page_nb: 0}).safeSubscribe(this, (res: any) => {
        a.notifications = res.notifications;
        a.total_notifications = 0;
        if (a.notifications.length) {
          a.notifications.forEach((item) => {
            if (item.notif.status !== 'displayed') {
              a.total_notifications++;
            }
          });
        }
        
        
        this.commonService.setUser(a);
        this.router.navigate(['/user', me.user.secure_id]);
        this.closeMenuPop();
      });
    });
  }


  logInPop() {
    this.logInPopRef = this.dialog.open(LoginPop, <MdDialogConfig>{
      position: {top: '30px', right: '30px'}
    });


    this.logInPopRef.afterClosed().safeSubscribe(this, res => {
      if (!!res) {
        this.closeMenuPop();
      }
    });
  }


  signInPop() {
    this.createAccPopRef = this.dialog.open(CreateAccPop, <MdDialogConfig>{
      position: {top: '30px', right: '30px'}
    });
  }


  creditsPop() {
    this.creditsPopRef = this.dialog.open(CreditsPop, <MdDialogConfig>{
      position: {top: '30px', right: '30px'}
    });
  }


  termsPop() {
    this.termsPopRef = this.dialog.open(TermsPop);
  }


  feedbackPop() {
    this.feedbackPopRef = this.dialog.open(FeedbackPop);
  }


  logOut() {
    this.api.logOut();
    this.closeMenuPop();
  }


  setLanguage(item) {
    const body = {
      current: item,
      arr: this.languages.arr
    };
  
    this.commonService.setLanguages(body);
    this.translate.use(item.id);
    localStorage.removeItem('O4PLang');
    localStorage.setItem('O4PLang', JSON.stringify(item.id));
    this.api.getNotifications();
  }
}
