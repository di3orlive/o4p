import {Component, Inject} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {MD_DIALOG_DATA, MdDialogRef} from '@angular/material';
import {Md2Toast} from 'md2';
import {CommonService, ApiService} from '../../services';
import {SafeSubscribe} from '../../helpers/safe-subscripe/safe-subscripe';



@Component({
  selector: 'app-update-user-bot',
  templateUrl: './update-user-bot.pop.html',
  styleUrls: ['./update-user-bot.pop.scss']
})
export class UpdateUserBotPop extends SafeSubscribe {
  update: any;
  languages: any;


  constructor(
    private commonService: CommonService,
    private api: ApiService,
    private translate: TranslateService,
    private toast: Md2Toast,
    public dialogRef: MdDialogRef<UpdateUserBotPop>,
    @Inject(MD_DIALOG_DATA) private user: any
  ) {
    super();
    this.initUpdate(this.user);
  }


  initUpdate(user) {
    this.update = Object.assign({}, user);

    this.commonService.languagesAsync.safeSubscribe(this, (value) => {
      this.languages = value;
      this.update.language = this.languages.current;
    });
    
    this.api.getNotifications();

    this.update.birthdate = user.birthdate || 'Mon Jan 01 1990 00:00:00 GMT+0000 (FLE Standard Time)';
  }

  save() {
    const body = {
      is_address_public: this.update.is_address_public,
      address: this.update.full_address,
      is_phone_public: this.update.is_phone_public,
      phone: this.update.phone,
      is_email_public: this.update.is_email_public,
      email: this.update.email,
      is_birthday_public: this.update.is_birthday_public,
      gender: this.update.gender,
      email_notifications: this.update.email_notifications,
      newsletter: this.update.newsletter,
      email_events: this.update.email_events,
      birthdate: this.update.birthdate,
      language_id: this.update.language.id,
      google_place_id: this.update.google_place_id,
    };


    this.toast.toast(this.translate.instant('user_updating'));
    this.dialogRef.close(body);
  }


  setAddress(e) {
    this.update.full_address = e.full_address;
    this.update.google_place_id = e.place_id;
  }
}
