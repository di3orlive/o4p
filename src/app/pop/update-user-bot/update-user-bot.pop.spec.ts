import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateUserBotPop } from './update-user-bot.pop';

describe('UpdateUserBotPop', () => {
  let component: UpdateUserBotPop;
  let fixture: ComponentFixture<UpdateUserBotPop>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateUserBotPop ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateUserBotPop);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
