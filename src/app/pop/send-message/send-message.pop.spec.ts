import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendMessagePop } from './send-message.pop';

describe('SendMessagePop', () => {
  let component: SendMessagePop;
  let fixture: ComponentFixture<SendMessagePop>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendMessagePop ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendMessagePop);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
