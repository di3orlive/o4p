import {Component, Inject, OnInit} from '@angular/core';
import {MD_DIALOG_DATA, MdDialogRef} from '@angular/material';
import {SafeSubscribe} from '../../helpers/safe-subscripe/safe-subscripe';
import {ApiService} from '../../services';

@Component({
  selector: 'app-send-message-pop',
  templateUrl: './send-message.pop.html',
  styleUrls: ['./send-message.pop.scss']
})
export class SendMessagePop extends SafeSubscribe implements OnInit {

  constructor(
    private api: ApiService,
    public dialogRef: MdDialogRef<SendMessagePop>,
    @Inject(MD_DIALOG_DATA) public data: any,
  ) {
    super();
  }


  ngOnInit() {
  }


  sendMessage() {
    console.log(this.data.type);
    if (this.data.type === 'to') {
      const body = {
        message: this.data.text
      };

      this.api.postPB(`events/${this.data.data.secure_id}/messages`, body).safeSubscribe(this, (res) => {
        this.dialogRef.close('YO');
      });
    } else {
      const body = {
        reply: this.data.text
      };

      this.api.postPB(`messages/${this.data.data.secure_id}/replies`, body).safeSubscribe(this, (res) => {
        this.dialogRef.close('YO');
      });
    }
  }
}
