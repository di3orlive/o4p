import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginPop } from './login.pop';

describe('LoginPop', () => {
  let component: LoginPop;
  let fixture: ComponentFixture<LoginPop>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginPop ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginPop);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
