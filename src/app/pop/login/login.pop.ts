import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {MdDialog, MdDialogRef, MdDialogConfig} from '@angular/material';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import * as sha256 from 'js-sha256/src/sha256.js';
import {SafeSubscribe} from '../../helpers/safe-subscripe/safe-subscripe';
import {ResetPassPop} from '../reset-pass/reset-pass.pop';
import {CreateAccPop} from '../create-acc/create-acc.pop';
import {ApiService, CommonService} from '../../services';


@Component({
  selector: 'app-login-pop',
  templateUrl: './login.pop.html',
  styleUrls: ['./login.pop.scss']
})
export class LoginPop extends SafeSubscribe implements OnInit {
  resetPopRef: MdDialogRef<ResetPassPop>;
  createAccPopRef: MdDialogRef<CreateAccPop>;
  form: FormGroup;

  constructor(
    public dialog: MdDialog,
    public dialogRef: MdDialogRef<LoginPop>,
    private api: ApiService,
    private commonService: CommonService,
    private router: Router,
  ) {
    super();
  }

  ngOnInit() {
    const login = new FormControl('', Validators.required);
    const password = new FormControl('', Validators.required);


    this.form = new FormGroup({
      login: login,
      password: password
    });
  }


  logInFunc() {
    if (this.form.invalid) { return null; }


    const body = {
      login: this.form.controls.login.value,
      password: sha256(this.form.controls.password.value),
      sessionType: 'o4pSession'
    };

    this.api.postPB(`sessions`, body).safeSubscribe(this, (res: any) => {

      const a = this.commonService.O4PUser.getValue();
      a.sessionToken = res.session.token;
      a.sessionType = body.sessionType;
      this.commonService.setUser(a);

      this.getMe();
    });
  }
  
  
  resetPop() {
    this.resetPopRef = this.dialog.open(ResetPassPop, <MdDialogConfig>{
      position: {top: '30px', right: '30px'}
    });
  }


  createAccPop() {
    this.createAccPopRef = this.dialog.open(CreateAccPop, <MdDialogConfig>{
      position: {top: '30px', right: '30px'}
    });
  }


  getMe() {
    const a = this.commonService.O4PUser.getValue();

    this.api.getP(`users/me`).safeSubscribe(this, (me: any) => {
      a.loggedIn = true;
      a.user = me.user;
      a.image = me.user.image;


      this.api.getPB(`notifications`, {page_nb: 0}).safeSubscribe(this, (res: any) => {
        a.notifications = res.notifications;
        a.total_notifications = 0;
        if (a.notifications.length) {
          a.notifications.forEach((item) => {
            if (item.notif.status !== 'displayed') {
              a.total_notifications++;
            }
          });
        }


        this.commonService.setUser(a);
        this.router.navigate(['/user', me.user.secure_id]);
        this.dialogRef.close('YO');
      });
    });
  }
}
