import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotiPop } from './noti.pop';

describe('NotiPop', () => {
  let component: NotiPop;
  let fixture: ComponentFixture<NotiPop>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotiPop ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotiPop);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
