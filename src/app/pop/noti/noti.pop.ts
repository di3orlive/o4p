import {Component} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {MdDialog, MdDialogConfig, MdDialogRef} from '@angular/material';
import {Md2Toast} from 'md2';
import {SafeSubscribe} from '../../helpers/safe-subscripe/safe-subscripe';
import {ApiService, CommonService} from '../../services';
import {SendMessagePop} from '../send-message/send-message.pop';

@Component({
  selector: 'app-noti-pop',
  templateUrl: './noti.pop.html',
  styleUrls: ['./noti.pop.scss']
})
export class NotiPop extends SafeSubscribe {
  sendMessagePopRef: MdDialogRef<SendMessagePop>;
  O4PUser: any;
  page_nb = 0;

  constructor(
    public dialog: MdDialog,
    private translate: TranslateService,
    private api: ApiService,
    private toast: Md2Toast,
    private commonService: CommonService,
    public dialogRef: MdDialogRef<NotiPop>,
  ) {
    super();
    // this.commonService.userAsync.safeSubscribe(this, (res) => {
    //   // console.log(res);
    //   if (Object.getOwnPropertyNames(res.notifications).length > 0) {
    //     this.O4PUser = res;
    //   }
    // });

    this.commonService.userAsync.safeSubscribe(this, (res) => {
      this.O4PUser = res;
    });
  }


  sendMessageTo(item) {
    try {
      // this.messageTo = item;
      this.sendMessagePopRef = this.dialog.open(SendMessagePop, <MdDialogConfig>{
        data: {
          data: item,
          type: 'from'
        }
      });


      this.sendMessagePopRef.afterClosed().safeSubscribe(this, res => {
        // console.log(res);
        if (!!res) {
          this.getNoti(0, true);
        }
      });

      this.updateNoti(item.secure_id);
    }catch (e) {
      this.toast.toast(this.translate.instant('error_in_user_info'));
    }
  }


  getNoti(page_nb, clear?) {
    this.page_nb = page_nb || 0;

    this.api.getPB(`notifications`, {page_nb: this.page_nb}).safeSubscribe(this, (res: any) => {
      if (Object.getOwnPropertyNames(res.notifications).length > 0) {
        const a = this.commonService.O4PUser.getValue();

        if (clear) {
          a.notifications = res.notifications;
        } else {
          a.notifications = this.arrayUnique(this.O4PUser.notifications.concat(res.notifications));
        }

        a.total_notifications = 0;
        if (a.notifications.length) {
          a.notifications.forEach((item) => {
            if (item.notif.status !== 'displayed') {
              a.total_notifications++;
            }
          });
        }
        this.commonService.setUser(a);
      } else {
        --this.page_nb;
      }
    });
  }


  updateNoti(secure_id) {
    this.api.putPB(`notifications/${secure_id}`, {status: 'displayed'}).safeSubscribe(this, (res) => {
      this.getNoti(0, true);
    });

    // mainService.updateNotifications({secure_id: secure_id, status: 'displayed'}, function() {
    //   $scope.getAllNotifi();
    // }, function(err) {
    //   $scope.showToast(err.data.message);
    // });
  }



  arrayUnique(array) {
    const a = array.concat();
    for (let i = 0; i < a.length; ++i) {
      for (let j = i + 1; j < a.length; ++j) {
        if (a[i].notif.secure_id === a[j].notif.secure_id) {
          a.splice(j--, 1);
        }
      }
    }
    return a;
  }
}
