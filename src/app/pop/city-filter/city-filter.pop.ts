import {Component, Inject, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {MD_DIALOG_DATA, MdDialogRef} from '@angular/material';
import {Md2Toast} from 'md2';

@Component({
  selector: 'app-city-filter-pop',
  templateUrl: './city-filter.pop.html',
  styleUrls: ['./city-filter.pop.scss']
})
export class CityFilterPop implements OnInit {
  city: any;
  // city = {
  //   isActive: false,
  //   full_address: '',
  //   place_id: '',
  //   gpsLatitude: '',
  //   gpsLongitude: ''
  // };

  constructor(
    private toast: Md2Toast,
    private translate: TranslateService,
    @Inject(MD_DIALOG_DATA) public data: any,
    public dialogRef: MdDialogRef<CityFilterPop>,
  ) {
    this.city = data;
  }

  ngOnInit() {
  }

  setPlace(e) {
    this.city = e;
  }

  closeSave() {
    this.checkCity();

    this.city.isActive = true;
    this.dialogRef.close(this.city);
  }

  close() {
    this.city = {};
    this.city.isActive = false;
    this.dialogRef.close(this.city);
  }


  checkCity() {
    if (!this.city.full_address) {
      this.toast.toast(this.translate.instant('please_pick_a_city'));
      return null;
    }
  }
}

