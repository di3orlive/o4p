import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CityFilterPop } from './city-filter.pop';

describe('CityFilterPop', () => {
  let component: CityFilterPop;
  let fixture: ComponentFixture<CityFilterPop>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CityFilterPop ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CityFilterPop);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
