import {Component, Inject, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {MD_DIALOG_DATA, MdDialogRef} from '@angular/material';
import {Md2Toast} from 'md2';
import {SafeSubscribe} from '../../helpers/safe-subscripe/safe-subscripe';
import {ApiService} from '../../services';

@Component({
  selector: 'app-media-slider',
  templateUrl: './media-slider.pop.html',
  styleUrls: ['./media-slider.pop.scss']
})
export class MediaSliderPop extends SafeSubscribe implements OnInit {
  media: any;


  constructor(
    private api: ApiService,
    private translate: TranslateService,
    @Inject(MD_DIALOG_DATA) public data: any,
    public dialogRef: MdDialogRef<MediaSliderPop>,
    private toast: Md2Toast
  ) {
    super();
  }

  ngOnInit() {
    this.media = this.data.mediaArr[this.data.mediaIndex];
  }


  changeMedia(dir) {
    switch (dir) {
      case 0:
        if (!this.data.mediaArr[this.data.mediaIndex - 1]) { return; }
        this.data.mediaIndex -= 1;
        this.media = this.data.mediaArr[this.data.mediaIndex];
        break;
      case 1:
        if (!this.data.mediaArr[this.data.mediaIndex + 1]) { return; }
        this.data.mediaIndex += 1;
        this.media = this.data.mediaArr[this.data.mediaIndex];
        break;
    }
  }


  deleteMedia(media) {
    if (media.user_media_secure_id) {
      this.data.mediaArr.splice(this.data.mediaIndex, 1);
      this.media = this.data.mediaArr[this.data.mediaArr.length < this.data.mediaIndex ? this.data.mediaIndex : this.data.mediaIndex - 1];
      this.data.mediaIndex--;


      this.api.deleteP(`users/medias/${media.user_media_secure_id}`).safeSubscribe(this, (res) => {
        this.toast.toast(this.translate.instant('media_deleted'));
      });
    } else if (media.event_media_secure_id) {
      this.data.mediaArr.splice(this.data.mediaIndex, 1);
      this.media = this.data.mediaArr[this.data.mediaArr.length < this.data.mediaIndex ? this.data.mediaIndex : this.data.mediaIndex - 1];
      this.data.mediaIndex--;


      this.api.deleteP(`events/medias/${media.event_media_secure_id}`).safeSubscribe(this, (res) => {
        this.toast.toast(this.translate.instant('media_deleted'));
      });
    }
  }
}
