import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MediaSliderPop } from './media-slider.pop';

describe('MediaSliderPop', () => {
  let component: MediaSliderPop;
  let fixture: ComponentFixture<MediaSliderPop>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MediaSliderPop ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MediaSliderPop);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
