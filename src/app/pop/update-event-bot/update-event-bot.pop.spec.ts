import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateEventBotPop } from './update-event-bot.pop';

describe('UpdateEventBotPop', () => {
  let component: UpdateEventBotPop;
  let fixture: ComponentFixture<UpdateEventBotPop>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateEventBotPop ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateEventBotPop);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
