import {Component, Inject} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {MD_DIALOG_DATA, MdDialogRef} from '@angular/material';
import {Md2Toast} from 'md2';
import {SafeSubscribe} from '../../helpers/safe-subscripe/safe-subscripe';
import {ApiService} from '../../services';

@Component({
  selector: 'app-update-event-bot',
  templateUrl: './update-event-bot.pop.html',
  styleUrls: ['./update-event-bot.pop.scss']
})
export class UpdateEventBotPop extends SafeSubscribe {
  isInvalidCondition = true;
  update: any;
  items = [];
  limitations = {
    crowd: {
      isActive: true,
      val: [0, 100]
    },
    gender: {
      isActive: true,
      val: 2,
    },
    age: {
      isActive: true,
      val: [18, 45]
    },
  };


  constructor(
    private toast: Md2Toast,
    private translate: TranslateService,
    public dialogRef: MdDialogRef<UpdateEventBotPop>,
    @Inject(MD_DIALOG_DATA) private event: any,
    private api: ApiService
  ) {
    super();
    this.initUpdate(this.event);
    this.initLimitations(this.event);
  }


  save() {
    this.checkCondition();

    if (this.isInvalidCondition) {
      this.toast.toast(this.translate.instant('please_fill_all_fields'));
      return null;
    }


    const isO4P = this.update.o4p === 'o4p';

    const body = {
      condition: this.update.condition,
      items: this.update.condition === 'list' ? this.items : null,
      price: this.update.condition === 'price' ? this.update.price : null,
      event_type: this.update.o4p === 'o4p' ? 'o4p' : this.update.o4p === 'limitations' ? this.update.event_type : 'error',

      nb_people_min: !isO4P && this.limitations.crowd.isActive ? this.limitations.crowd.val[0] : null,
      nb_people_max: !isO4P && this.limitations.crowd.isActive ? this.limitations.crowd.val[1] : null,
      gender_balance: !isO4P && this.limitations.gender.isActive ? this.limitations.gender.val : null,
      age_min: !isO4P && this.limitations.age.isActive ? this.limitations.age.val[0] : null,
      age_max: !isO4P && this.limitations.age.isActive ? this.limitations.age.val[1] : null,

      options: this.update.options_output && this.update.options_output[0] != null ? this.update.options_output : [],
      description: this.update.description
    };


    this.toast.toast(this.translate.instant('event_updating'));

    this.dialogRef.close(body);
  }

  initUpdate(event) {
    this.update = Object.assign({}, event);

    if (event.items != null) {
      this.items = event.items;
    } else {
      this.addListRow();
    }
  }

  initLimitations(event) {
    if (event.nb_people_min == null || event.nb_people_max == null) {
      this.limitations.crowd.isActive = false;
    } else {
      this.limitations.crowd.val[0] = event.nb_people_min;
      this.limitations.crowd.val[1] = event.nb_people_max;
    }

    if (event.gender_balance == null) {
      this.limitations.gender.isActive = false;
    } else {
      this.limitations.gender.val = event.gender_balance;
    }

    if (event.age_min == null || event.age_max == null) {
      this.limitations.age.isActive = false;
    } else {
      this.limitations.age.val[0] = event.age_min;
      this.limitations.age.val[1] = event.age_max;
    }
  }

  checkCondition() {
    if (this.update.condition === 'price') {
      if (this.update.price) {
        this.isInvalidCondition = false;
        return null;
      }
    } else if (this.update.condition === 'list') {
      if (this.items[0] != null && !!this.items[0].label && !!this.items[0].unit && !!this.items[0].quantity) {
        this.isInvalidCondition = false;
        return null;
      }
    } else {
      if (this.update.condition) {
        this.isInvalidCondition = false;
        return null;
      }
    }

    this.isInvalidCondition = true;
  }

  onOption(e) {
    console.log(e);

    const options = [];
    for (let i = 0; i < e.length; i++) {
      if (e[i].isactive) {
        options.push(e[i].name);
      }
    }

    this.update.options = e;
    this.update.options_output = options;
  }

  crowdChange(e) {
    this.limitations.crowd = e;
  }

  genderChange(e) {
    this.limitations.gender = e;
  }

  ageChange(e) {
    this.limitations.age = e;
  }

  addListRow() {
    this.items.push({label: '', unit: '', quantity: ''});
  }

  removeListRow(item, index) {
    if (index === 0) { return null; }

    this.api.deleteP(`events/items/${item.item_secure_id}`).safeSubscribe(this, (res) => {
      this.items.splice(index, 1);
    });
  }
}

