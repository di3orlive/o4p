import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParticipationPop } from './participation.component';

describe('ParticipationPop', () => {
  let component: ParticipationPop;
  let fixture: ComponentFixture<ParticipationPop>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParticipationPop ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParticipationPop);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
