import {Component, Inject, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {SafeSubscribe} from '../../helpers/safe-subscripe/safe-subscripe';
import {MD_DIALOG_DATA, MdDialogRef} from '@angular/material';
import {Md2Toast} from 'md2';
import {ApiService, CommonService} from '../../services';

@Component({
  selector: 'app-participation',
  templateUrl: './participation.component.html',
  styleUrls: ['./participation.component.scss']
})
export class ParticipationPop extends SafeSubscribe implements OnInit {
  event: any;
  participation: any;
  O4PUser: any;
  tab = 1;
  users = [];
  email = '';
  skipSStep: any;
  appPpcobtn: any;
  appPpcobtn_val: any;
  amount_including_Ppfees: any;
  parameters: any;
  
  
  constructor(public dialogRef: MdDialogRef<ParticipationPop>,
              @Inject(MD_DIALOG_DATA) private data: any,
              private api: ApiService,
              private translate: TranslateService,
              private commonService: CommonService,
              private toast: Md2Toast
  ) {
    super();
    this.event = data.event;
    
    this.commonService.userAsync.safeSubscribe(this, (value) => {
      this.O4PUser = value;
    });
  
  
  
    this.commonService.parametersAsync.safeSubscribe(this, (value) => {
      this.parameters = value.parameters;
    });
  }
  
  
  ngOnInit() {
    this.addParticipation(this.data.status);
  }
  
  
  addParticipation(status) {
    if (status === 'temp') {
      this.api.postPB(`events/${this.event.secure_id}/participations`, {
        secure_id: this.event.secure_id,
        status: status
      }).safeSubscribe(this, (res: any) => {
        if (!res.success) {
          this.dialogRef.close();
          return;
        }
  
  
        this.skipSStep = this.event.condition === 'byos' || this.event.condition === 'free' || this.event.condition === 'list';
        
        this.participation = res.participation;
  
        if (this.participation.payment_details.charge_wallet) {
          this.api.postPB(`wallet_request`, {}).safeSubscribe(this, (res: any) => {
  
            console.log(this.appPpcobtn_val);
            
            this.appPpcobtn_val = this.parameters.wallet_min_charge > this.participation.payment_details.total_amount - this.O4PUser.user.wallet ? this.parameters.wallet_min_charge : this.participation.payment_details.total_amount - this.O4PUser.user.wallet;
            
            
            this.appPpcobtn = {custom: res.wallet.secure_id, parameters: this.parameters};
            this.calcAmountPpfees();
          });
        }
  
        
        setTimeout(() => {
          if (this.tab === 1) {
            this.dialogRef.close();
          }
        }, 1200000);
      });
    } else {
      const body = {
        secure_id: this.event.secure_id,
        status: status,
        items: this.event.condition === 'list' ? [] : null
      };
      
      if (this.event.condition === 'list') {
        const items = [];
        
        this.event.items.forEach((item) => {
          if (item.going_quantity && item.going_quantity !== 0) {
            items.push({secure_id: item.item_secure_id, quantity: `${item.going_quantity}`});
          }
        });
        
        body.items = items;
      }
      
      this.api.putPB(`participations/${this.participation.secure_id}`, body).safeSubscribe(this, (res) => {
        console.log(res);
      });
    }
  }
  
  
  calcAmountPpfees() {
    console.log(1);
    this.amount_including_Ppfees = (this.appPpcobtn_val + 0.35) / (1 - 0.034);
    this.amount_including_Ppfees = Math.round(this.amount_including_Ppfees * 100) / 100;
  }
  
  
  setTab(num) {
    this.tab = num;
    
    if (num === 3) {
      // if (this.event.condition === 'price') {
      //   this.api.putPB(`participations/${this.participation.secure_id}`, body).safeSubscribe(this, );
      // }
      
      this.addParticipation('waiting_maker');
    }
  }
  
  
  checkFStep() {
    if (this.event.condition === 'list' && this.event.items) {
      for (let i = 0; i < this.event.items.length; i++) {
        if (this.event.items[i].going_quantity && this.event.items[i].going_quantity !== 0) {
          return false;
        }
      }
      return true;
    }
  }
  
  
  checkSStep() {
    if (this.event.condition === 'price') {
      return true;
    }
  }
  
  
  closePop() {
    this.dialogRef.close('YO');
  }
  
  
  sendInvitations() {
    const usersArr = [];
    
    if (this.users[0]) {
      for (let i = 0; i < this.users.length; i++) {
        usersArr.push(this.users[i].username);
      }
      
      const body = {
        invitees: usersArr
      };
      
      this.api.postPB(`events/${this.data.secure_id}/invitations/`, body).safeSubscribe(this, () => {
        this.toast.toast(this.translate.instant('invitation_sent'));
        this.dialogRef.close();
      });
    } else {
      this.toast.toast(this.translate.instant('you_need_invite_at_least_one_friend'));
    }
  }
  
  onUser(e) {
    this.users = e;
  }
  
  
  chargeSuccess(e) {
    this.api.getUser(this.O4PUser.user.secure_id).safeSubscribe(this);
  
    const body = {
      data: e,
      request_secure_id: this.appPpcobtn.custom,
    };
    
    this.api.postPB(`charge_wallet`, body).safeSubscribe(this, (res) => {
      console.log(res);
    });
    
    this.participation.payment_details.charge_wallet = false;
  }
}
