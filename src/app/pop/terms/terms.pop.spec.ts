import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TermsPop } from './terms.pop';

describe('TermsPop', () => {
  let component: TermsPop;
  let fixture: ComponentFixture<TermsPop>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TermsPop ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TermsPop);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
