import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TourPop } from './tour.component';

describe('TourPop', () => {
  let component: TourPop;
  let fixture: ComponentFixture<TourPop>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TourPop ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TourPop);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
