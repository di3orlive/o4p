import { Component, OnInit } from '@angular/core';
import {MdDialogRef} from '@angular/material';
import {CommonService} from "../../services/common/common.service";
import {SafeSubscribe} from "../../helpers/safe-subscripe/safe-subscripe";

@Component({
  selector: 'app-tour',
  templateUrl: './tour.component.html',
  styleUrls: ['./tour.component.scss']
})
export class TourPop extends SafeSubscribe implements OnInit {
  tab = 1;
  languages: any;

  constructor(
    public dialogRef: MdDialogRef<TourPop>,
    private commonService: CommonService
  ) {
    super();
  
    this.commonService.languagesAsync.safeSubscribe(this, (value) => {
      this.languages = value;
    });
  }

  ngOnInit() {
  }
  
  
  setTab(num) {
    this.tab = num;
  }
  

  close() {
    this.dialogRef.close('YO');
  }
  
  
  setLanguage(item) {
    const body = {
      current: item,
      arr: this.languages.arr
    };
    
    this.commonService.setLanguages(body);
  }
}
