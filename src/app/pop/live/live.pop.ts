import { Component, OnInit } from '@angular/core';

declare const YT;

@Component({
  selector: 'app-live',
  templateUrl: './live.pop.html',
  styleUrls: ['./live.pop.scss']
})
export class LivePop implements OnInit {
  player: any;

  constructor() { }


  ngOnInit() {
    this.onYouTubeIframeAPIReady();
  }

  onYouTubeIframeAPIReady() {
    const checkMap = setInterval(() => {
      if ((typeof YT !== 'undefined') && YT && YT.Player) {
        clearInterval(checkMap);
        this.player = new YT.Player('live', {
          videoId: 'zzv2sKJ3aqE',
        });
      }
    }, 10);
  }
}
