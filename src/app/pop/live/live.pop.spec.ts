import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LivePop } from './live.pop';

describe('LivePop', () => {
  let component: LivePop;
  let fixture: ComponentFixture<LivePop>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LivePop ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LivePop);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
