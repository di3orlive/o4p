import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddMediaPop } from './add-media.pop';

describe('AddMediaPop', () => {
  let component: AddMediaPop;
  let fixture: ComponentFixture<AddMediaPop>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddMediaPop ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMediaPop);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
