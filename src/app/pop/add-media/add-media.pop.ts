import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {MdDialogRef} from '@angular/material';
import {Md2Toast} from 'md2';

@Component({
  selector: 'app-add-media',
  templateUrl: './add-media.pop.html',
  styleUrls: ['./add-media.pop.scss']
})
export class AddMediaPop implements OnInit {
  file = {
    userText: '',
    arr: []
  };


  constructor(
    private toast: Md2Toast,
    private translate: TranslateService,
    public dialogRef: MdDialogRef<AddMediaPop>
  ) { }


  ngOnInit() {
  }


  onFileChange(e) {
    this.file = e;
  }


  submitUpload(f) {
    if (this.file.arr.length < 1) {
      this.toast.toast(this.translate.instant('you_need_to_add_a_media_file'));
      return null;
    }
    if (f.invalid) {
      this.toast.toast(this.translate.instant('you_need_to_fill_in_all_the_required_fields'));
      return null;
    }

    const formData = new FormData();

    this.file.arr.forEach((item) => {
      formData.append('file', item.file);
    });

    this.toast.toast(this.translate.instant('media_uploading'));

    this.dialogRef.close({
      title: this.file.userText,
      formData: formData
    });

    // this.file.arr = [];
    // this.file.userText = null;
  }
}

