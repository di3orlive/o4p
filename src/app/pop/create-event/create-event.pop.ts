import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {MdDialogRef} from '@angular/material';
import {Md2Toast} from 'md2';
import {SafeSubscribe} from '../../helpers/safe-subscripe/safe-subscripe';
import {ApiService} from '../../services';


@Component({
  selector: 'app-create-event-pop',
  templateUrl: './create-event.pop.html',
  styleUrls: ['./create-event.pop.scss']
})
export class CreateEventPop extends SafeSubscribe implements OnInit {
  startDateTime: Date;
  endDateTime: Date;
  minStartDate = new Date();

  newEvent = {
    file: '',
    title: '',
    condition: {
      value: '',
      price: null,
      list: [{
        item: '',
        unit: '',
        quantity: ''
      }]
    },
    cover_picture: null,
    limitations: {
      value: 'o4p',
      crowd: {
        isActive: true,
        val: [0, 50]
      },
      gender: {
        isActive: true,
        val: 2
      },
      age: {
        isActive: true,
        val: [18, 45]
      },
      invitation: 'public',
      is_address_public: true
    },
    details: {
      address: {
        val: '',
        location: {
          place_id: '',
          gpsLatitude: '',
          gpsLongitude: ''
        }
      },
      startDate: this.startDateTime,
      startTime: '',
      endDate: this.endDateTime,
      endTime: '',
      description: ''
    },
    options: [],
    samples: [
      {media: {uri: 'assets/i/icons/user-api.png', secure_id: 1, type: 'img'}},
      {media: {uri: 'assets/i/icons/periscope.png', secure_id: 1, type: 'img'}}
    ],
    event_nature: 'fiesta'
  };
  file = {
    arr: []
  };
  tab = 1;
  firstStepValid = true;


  constructor(
    public dialogRef: MdDialogRef<CreateEventPop>,
    private api: ApiService,
    private translate: TranslateService,
    private toast: Md2Toast
  ) {
    super();
  }


  ngOnInit(): void {
    this.api.getP(`sample_pictures`).safeSubscribe(this, (resp: any) => {
      this.newEvent.samples = resp.samples;
    });
  }

  createEventFunc(f) {
    if (this.file.arr.length < 1) {
      this.toast.toast(this.translate.instant('you_need_to_add_a_media_file'));
      return null;
    }

    if (f.invalid) {
      if (f.controls.dateTimeStart.invalid) {
        f.controls.dateTimeStart.setErrors({ 'dirty': true });
      }

      this.toast.toast(this.translate.instant('you_need_to_fill_in_all_the_required_fields'));
      return;
    }
    
    if (!this.newEvent.details.address.location.place_id) {
      this.toast.toast(this.translate.instant('wrong_address'));
      return;
    }

    const isO4P = this.newEvent.limitations.value === 'o4p';


    const body = {
      body: {
        title: this.newEvent.title,
        description: this.newEvent.details.description,
        start_date: this.newEvent.details.startDate,
        condition: this.newEvent.condition.value,
        cover_picture: '',
        google_place_id: this.newEvent.details.address.location.place_id,
        gpsLatitude: this.newEvent.details.address.location.gpsLatitude,
        gpsLongitude: this.newEvent.details.address.location.gpsLongitude,

        end_date: !!this.newEvent.details.endDate ? this.newEvent.details.endDate : null,
        price: this.newEvent.condition.value === 'price' ? this.newEvent.condition.price : null,
        nb_people_min: !isO4P && this.newEvent.limitations.crowd.isActive ? this.newEvent.limitations.crowd.val[0] : null,
        nb_people_max: !isO4P && this.newEvent.limitations.crowd.isActive ? this.newEvent.limitations.crowd.val[1] : null,
        gender_balance: !isO4P && this.newEvent.limitations.gender.isActive ? this.newEvent.limitations.gender.val : null,
        age_min: !isO4P && this.newEvent.limitations.age.isActive ? this.newEvent.limitations.age.val[0] : null,
        age_max: !isO4P && this.newEvent.limitations.age.isActive ? this.newEvent.limitations.age.val[1] : null,
        items: this.newEvent.condition.value === 'list' ? this.newEvent.condition.list : null,

        options: this.newEvent.options[0] != null ? this.newEvent.options : [],

        event_type: !isO4P ? this.newEvent.limitations.invitation : 'o4p',
        event_nature: this.newEvent.event_nature,
      },
      file: this.file
    };
  
    console.log(body);
  
  
    this.toast.toast(this.translate.instant('event_creating'));

    this.dialogRef.close(body);
  }

  onFileChange(e) {
    this.file = e;
  }

  onOption(e) {
    const options = [];
    for (let i = 0; i < e.length; i++) {
      if (e[i].isactive) {
        options.push(e[i].name);
      }
    }

    this.newEvent.options = options;
  }

  onNatureSet(arr) {
    arr.forEach((it) => {
      if (it.isactive) {
        this.newEvent.event_nature = it.name;
      }
    });
  }

  setDefImg(item) {
    this.file.arr[0] = {
      img: item.uri,
      secure_id: item.secure_id,
      file: {
        type: item.type
      }
    };
  }

  crowdChange(e) {
    this.newEvent.limitations.crowd = e;
  }

  genderChange(e) {
    this.newEvent.limitations.gender = e;
  }

  ageChange(e) {
    this.newEvent.limitations.age = e;
  }

  checkFStep() {
    if (this.newEvent.condition.value === 'price') {
      if (this.newEvent.condition.price) {
        this.firstStepValid = false;
        return null;
      }
    } else if (this.newEvent.condition.value === 'list') {
      if (this.newEvent.condition.list[0] != null && !!this.newEvent.condition.list[0].item && !!this.newEvent.condition.list[0].unit && !!this.newEvent.condition.list[0].quantity) {
        this.firstStepValid = false;
        return null;
      }
    } else {
      if (this.newEvent.condition.value) {
        this.firstStepValid = false;
        return null;
      }
    }

    this.firstStepValid = true;
  }

  setTab(tab) {
    this.tab = tab;
  }

  setPlace(e) {
    this.newEvent.details.address.location = e;
  }

  addListRow() {
    this.newEvent.condition.list.push({item: '', unit: '', quantity: ''});
  }

  removeListRow(index) {
    if (index === 0) { return null; }
    this.newEvent.condition.list.splice(index, 1);
  }
}

