import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateEventPopComponent } from './create-event.pop';

describe('CreateEventPopComponent', () => {
  let component: CreateEventPopComponent;
  let fixture: ComponentFixture<CreateEventPopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateEventPopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateEventPopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
