import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResetPassPop } from './reset-pass.pop';

describe('ResetPassPop', () => {
  let component: ResetPassPop;
  let fixture: ComponentFixture<ResetPassPop>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResetPassPop ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResetPassPop);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
