import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MdDialogRef} from '@angular/material';
import {Md2Toast} from 'md2';
import {ApiService} from '../../services/api/api.service';
import {SafeSubscribe} from '../../helpers/safe-subscripe/safe-subscripe';

@Component({
  selector: 'app-reset-pass-pop',
  templateUrl: './reset-pass.pop.html',
  styleUrls: ['./reset-pass.pop.scss']
})
export class ResetPassPop extends SafeSubscribe implements OnInit {
  form: FormGroup;

  constructor(
    private api: ApiService,
    private translate: TranslateService,
    public dialogRef: MdDialogRef<ResetPassPop>,
    private toast: Md2Toast
  ) {
    super();
    this.form = new FormGroup({
      login: new FormControl(null, Validators.required)
    });
  }

  ngOnInit() {
  }

  resetPass() {
    if (this.form.invalid) { return null; }

    const body = {
      login: this.form.controls.login.value
    };

    this.api.postPB(`password_reset`, body).safeSubscribe(this, (res) => {
      this.toast.toast(this.translate.instant('check_your_mail'));
      this.dialogRef.close();
    });
  }
}
