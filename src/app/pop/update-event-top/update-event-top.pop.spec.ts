import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateEventTopPop } from './update-event-top.pop';

describe('UpdateEventTopPop', () => {
  let component: UpdateEventTopPop;
  let fixture: ComponentFixture<UpdateEventTopPop>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateEventTopPop ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateEventTopPop);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
