import {Component, Inject} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {MD_DIALOG_DATA, MdDialog, MdDialogRef, MdDialogConfig} from '@angular/material';
import {Router} from '@angular/router';
import {Md2Toast} from 'md2';
import {SafeSubscribe} from '../../helpers/safe-subscripe/safe-subscripe';
import {ConfirmPop} from '../confirm/confirm.pop';
import {ApiService} from '../../services';

@Component({
  selector: 'app-update-event-top',
  templateUrl: './update-event-top.pop.html',
  styleUrls: ['./update-event-top.pop.scss']
})
export class UpdateEventTopPop extends SafeSubscribe {
  deleteConfirmDialogRef: MdDialogRef<ConfirmPop>;
  update: any;
  file = {
    arr: []
  };


  constructor(
    private dialog: MdDialog,
    private translate: TranslateService,
    public dialogRef: MdDialogRef<UpdateEventTopPop>,
    @Inject(MD_DIALOG_DATA) private event: any,
    private toast: Md2Toast,
    private router: Router,
    private api: ApiService
  ) {
    super();
    this.initUpdate(this.event);
  }



  initUpdate(event) {
    this.update = Object.assign({}, event);
    this.update.start_date =  new Date(event.start_date);
    this.update.end_date = event.end_date ? new Date(event.end_date) : null;

    this.update.media_secure_id = event.medias[0].media_secure_id;
    this.file.arr.push({file: '', img: event.image, media_secure_id: event.medias[0].media_secure_id});

    event.event_nature.forEach((it) => {
      if (it.isactive) {
        this.update.event_nature_val = it.name;
      }
    });
  }


  save(f) {
    if (this.file.arr.length < 1) {
      this.toast.toast(this.translate.instant('you_need_to_add_a_media_file'));
      return null;
    }

    if (f.invalid) {
      if (f.controls.dateTimeStart.invalid) {
        f.controls.dateTimeStart.setErrors({ 'dirty': true });
      }
      this.toast.toast(this.translate.instant('you_need_to_fill_in_all_the_required_fields'));
      return null;
    }


    const body = {
      body: {
        title: this.update.title,
        event_nature: this.update.event_nature_val,
        start_date: this.update.start_date,
        end_date: this.update.end_date,
        google_place_id: this.update.google_place_id,
        media_secure_id: this.update.media_secure_id
      },
      file: this.file
    };

    this.toast.toast(this.translate.instant('event_updating'));

    this.dialogRef.close(body);
  }


  setPlace(e) {
    this.update.city_country = e.city_country;
    this.update.google_place_id = e.place_id;
  }


  onFileChange(e) {
    this.file = e;
  }


  onNatureSet(arr) {
    arr.forEach((it) => {
      if (it.isactive) {
        this.update.event_nature_val = it.name;
      }
    });
  }


  deleteConfirmPop() {
    this.deleteConfirmDialogRef = this.dialog.open(ConfirmPop, <MdDialogConfig>{
      data: {
        title: this.translate.instant('confirm_event_deleting')
      }
    });

    this.deleteConfirmDialogRef.afterClosed().safeSubscribe(this, res => {
      if (!!res) {
        this.api.deleteP(`events/${this.event.secure_id}`).safeSubscribe(this, () => {
          this.dialogRef.close();
          this.router.navigate(['']);
        });
      }
    });
  }
}


