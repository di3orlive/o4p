import {Component, Inject, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {MD_DIALOG_DATA, MdDialogRef} from '@angular/material';
import {Md2Toast} from 'md2';
import {SafeSubscribe} from '../../helpers/safe-subscripe/safe-subscripe';
import {ApiService} from '../../services';

@Component({
  selector: 'app-live-desc',
  templateUrl: './live-desc.pop.html',
  styleUrls: ['./live-desc.pop.scss']
})
export class LiveDescPop extends SafeSubscribe implements OnInit {
  periscope_id: any;

  constructor(
    private api: ApiService,
    private translate: TranslateService,
    private toast: Md2Toast,
    public dialogRef: MdDialogRef<LiveDescPop>,
    @Inject(MD_DIALOG_DATA) public event: any,
  ) {
    super();
  }

  ngOnInit() {
  }


  liveCam() {
    if (!this.periscope_id) {
      this.toast.toast(this.translate.instant('need_periscope_id'));
      return null;
    }

    const body = {
      periscope_id: this.periscope_id
    };

    this.api.postPB(`events/${this.event.secure_id}/live_cam`, body).safeSubscribe(this, (res: any) => {
      if (res.ok) {
        this.toast.toast(this.translate.instant('periscope_id_is_set'));
      }
      this.dialogRef.close('YO');
    });
  }
}
