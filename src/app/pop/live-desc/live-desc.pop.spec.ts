import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LiveDescPop } from './live-desc.pop';

describe('LiveDescPop', () => {
  let component: LiveDescPop;
  let fixture: ComponentFixture<LiveDescPop>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LiveDescPop ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LiveDescPop);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
