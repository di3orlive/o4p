import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeedbackPop } from './feedback.pop';

describe('FeedbackPop', () => {
  let component: FeedbackPop;
  let fixture: ComponentFixture<FeedbackPop>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeedbackPop ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeedbackPop);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
