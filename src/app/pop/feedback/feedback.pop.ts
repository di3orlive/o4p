import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MdDialogRef} from '@angular/material';
import {Md2Toast} from 'md2';
import {SafeSubscribe} from '../../helpers/safe-subscripe/safe-subscripe';
import {ApiService} from '../../services';

@Component({
  selector: 'app-feedback-pop',
  templateUrl: './feedback.pop.html',
  styleUrls: ['./feedback.pop.scss']
})
export class FeedbackPop extends SafeSubscribe implements OnInit {
  form: FormGroup;

  constructor(
    public dialogRef: MdDialogRef<FeedbackPop>,
    private translate: TranslateService,
    private api: ApiService,
    private toast: Md2Toast
  ) {
    super();
    const email = new FormControl(null, [Validators.required, Validators.pattern(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/)]);
    const firstname = new FormControl('', Validators.required);
    const lastname = new FormControl('', Validators.required);
    const message = new FormControl('', [Validators.required, Validators.maxLength(1500)]);


    this.form = new FormGroup({
      email: email,
      firstname: firstname,
      lastname: lastname,
      message: message
    });
  }


  ngOnInit() {
  }


  sendFeed() {
    const body = {
      email: this.form.controls.email.value,
      firstname: this.form.controls.firstname.value,
      lastname: this.form.controls.lastname.value,
      message: this.form.controls.message.value
    };

    this.api.postPB(`email`, body).safeSubscribe(this, (res) => {
      console.log(res);

      this.toast.toast(this.translate.instant('feedback_has_been_sent'));

      this.dialogRef.close();
    });
  }
}
