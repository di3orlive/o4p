import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditsPop } from './credits.pop';

describe('CreditsPop', () => {
  let component: CreditsPop;
  let fixture: ComponentFixture<CreditsPop>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreditsPop ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditsPop);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
