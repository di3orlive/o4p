import { Component, OnInit } from '@angular/core';
import {MdDialog, MdDialogRef, MdDialogConfig} from '@angular/material';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {Md2Toast} from 'md2';
import {SafeSubscribe} from '../../helpers/safe-subscripe/safe-subscripe';
import {CommonService, ApiService} from '../../services';
import {CreditsChargePop} from '../credits-charge/credits-charge.component';
import {ClaimMoneyPop} from '../claim-money/claim-money.component';
import {TransactionsComponent} from '../transactions/transactions.component';

@Component({
  selector: 'app-credits-pop',
  templateUrl: './credits.pop.html',
  styleUrls: ['./credits.pop.scss']
})
export class CreditsPop extends SafeSubscribe implements OnInit {
  claimMoneyDialogRef: MdDialogRef<ClaimMoneyPop>;
  addCardDialogRef: MdDialogRef<CreditsChargePop>;
  transitionsDialogRef: MdDialogRef<TransactionsComponent>;
  O4PUser: any;
  form: FormGroup;


  constructor(
    public dialog: MdDialog,
    public dialogRef: MdDialogRef<CreditsPop>,
    private translate: TranslateService,
    private commonService: CommonService,
    private api: ApiService,
    private toast: Md2Toast
  ) {
    super();
    this.commonService.userAsync.safeSubscribe(this, (value) => {
      this.O4PUser = value;
  
  
      this.form = new FormGroup({
        email: new FormControl(this.O4PUser.user.paypal_email_address, [Validators.required, Validators.pattern(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/)])
      });
    });
  }

  ngOnInit() {
  }


  savePPAddress() {
    if (this.form.invalid) { return null; }
  
  
    const body = {
      fields: {
        paypal_email_address: this.form.controls.email.value
      }
    };
  
  
  
    this.toast.toast(this.translate.instant('user_updating'));
    
    this.api.putPB(`users/${this.O4PUser.user.secure_id}`, body).safeSubscribe(this, (userres: any) => {
      if (userres._body) {
        const body = JSON.parse(userres._body);
        if (body.statusText && body.statusText !== 'OK') {
          this.toast.toast(body.statusText);
          return;
        }
      }
  
      this.api.getUser(this.O4PUser.user.secure_id).safeSubscribe(this);
    
      if (userres.status !== 403) {
        this.toast.toast(this.translate.instant('user_updated'));
      }
    });
  }
  
  creditsChargePop() {
    this.addCardDialogRef = this.dialog.open(CreditsChargePop, <MdDialogConfig>{
      position: {top: '30px', right: '30px'}
    });
  }

  claimMoneyPop() {
    this.claimMoneyDialogRef = this.dialog.open(ClaimMoneyPop, <MdDialogConfig>{
      position: {top: '30px', right: '30px'}
    });
  
    this.claimMoneyDialogRef.afterClosed().safeSubscribe(this, (res: any) => {
      if (!!res) {
        console.log(res);
        this.api.postPB(`claim_money`, {amount: res}).safeSubscribe(this);
      }
    });
  }
  
  transitionsPop() {
    this.transitionsDialogRef = this.dialog.open(TransactionsComponent);
  
    this.transitionsDialogRef.afterClosed().safeSubscribe(this, (res: any) => {
      if (!!res) {
        console.log(res);
      }
    });
  }
}
