import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAccPop } from './create-acc.pop';

describe('CreateAccPop', () => {
  let component: CreateAccPop;
  let fixture: ComponentFixture<CreateAccPop>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateAccPop ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAccPop);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
