import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {MdDialog, MdDialogRef} from '@angular/material';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Md2Toast} from 'md2';
import * as sha256 from 'js-sha256/src/sha256.js';
import {CustomValidators} from 'ng2-validation/dist';
import {SafeSubscribe} from '../../helpers/safe-subscripe/safe-subscripe';
import {ApiService} from '../../services';
import {TermsPop} from '../terms/terms.pop';


@Component({
  selector: 'app-create-acc-pop',
  templateUrl: './create-acc.pop.html',
  styleUrls: ['./create-acc.pop.scss']
})
export class CreateAccPop extends SafeSubscribe implements OnInit {
  termsPopRef: MdDialogRef<TermsPop>;
  form: FormGroup;
  recaptcha = true;

  constructor(
    public dialog: MdDialog,
    public api: ApiService,
    private translate: TranslateService,
    public dialogRef: MdDialogRef<CreateAccPop>,
    private toast: Md2Toast
  ) {
    super();
  }

  ngOnInit() {
    const email = new FormControl('', [Validators.required, Validators.pattern(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/)]);
    const password = new FormControl('', Validators.required);
    const password2 = new FormControl('', [Validators.required, CustomValidators.equalTo(password)]);
    const username = new FormControl('', Validators.required);
    const terms = new FormControl('', Validators.required);


    this.form = new FormGroup({
      email: email,
      password: password,
      password2: password2,
      username: username,
      terms: terms,
    });
  }


  createAcc() {
    if (this.form.invalid && !this.recaptcha) { return null; }


    const body = {
      email: this.form.controls.email.value,
      username: this.form.controls.username.value,
      password: sha256(this.form.controls.password.value),
      recaptcha_response: this.recaptcha
    };


    this.api.postPB(`users`, body).safeSubscribe(this, (res) => {
      console.log(res);
      this.toast.toast(this.translate.instant('check_your_mail'));
      this.dialogRef.close();
    });
  }
  
  
  
  termsPop() {
    this.termsPopRef = this.dialog.open(TermsPop);
  }
  
  
  onRecaptcha(e) {
    this.recaptcha = e;
  }
}
