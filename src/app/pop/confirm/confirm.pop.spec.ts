import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmPop } from './confirm.pop';

describe('ConfirmPop', () => {
  let component: ConfirmPop;
  let fixture: ComponentFixture<ConfirmPop>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmPop ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmPop);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
