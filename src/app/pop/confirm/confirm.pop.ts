import {Component, Inject, OnInit} from '@angular/core';
import {MD_DIALOG_DATA, MdDialogRef} from '@angular/material';

@Component({
  selector: 'app-delete-confirm',
  templateUrl: './confirm.pop.html',
  styleUrls: ['./confirm.pop.scss']
})
export class ConfirmPop implements OnInit {

  constructor(
    public dialogRef: MdDialogRef<ConfirmPop>,
    @Inject(MD_DIALOG_DATA) public user: any
  ) { }

  ngOnInit() {
  }

  confirm(bool) {
    this.dialogRef.close(bool);
  }
}
