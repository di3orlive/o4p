import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MdDialogRef} from '@angular/material';
import {SafeSubscribe} from '../../helpers/safe-subscripe/safe-subscripe';
import {ApiService} from '../../services/api/api.service';
import {CommonService} from '../../services';

@Component({
  selector: 'app-credits-charge',
  templateUrl: './credits-charge.component.html',
  styleUrls: ['./credits-charge.component.scss']
})
export class CreditsChargePop extends SafeSubscribe implements OnInit {
  form: FormGroup;
  O4PUser: any;
  appPpcobtn: any;
  parameters: any;

  constructor(
    public dialogRef: MdDialogRef<CreditsChargePop>,
    private commonService: CommonService,
    private api: ApiService
  ) {
    super();
    this.commonService.userAsync.safeSubscribe(this, (value) => {
      this.O4PUser = value;
    });
  
  
    this.commonService.parametersAsync.safeSubscribe(this, (value) => {
      this.parameters = value.parameters;
    });
    

    this.form = new FormGroup({
      num: new FormControl(null, Validators.required)
    });
  }

  ngOnInit() {
    this.appPpcobtn = {custom: this.O4PUser.user.secure_id, parameters: this.parameters};
  }
  
  
  chargeSuccess(e) {
    this.api.getUser(this.O4PUser.user.secure_id).safeSubscribe(this);
  
    const body = {
      data: e,
      request_secure_id: this.appPpcobtn.custom,
    };
  
    this.api.postPB(`charge_wallet`, body).safeSubscribe(this, (res) => {
      console.log(res);
    });
    
    this.dialogRef.close();
  }
}
