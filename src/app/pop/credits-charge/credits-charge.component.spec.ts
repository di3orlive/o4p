import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditsChargePop } from './credits-charge.component';

describe('CreditsChargePop', () => {
  let component: CreditsChargePop;
  let fixture: ComponentFixture<CreditsChargePop>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreditsChargePop ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditsChargePop);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
