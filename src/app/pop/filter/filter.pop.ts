import {Component, OnInit, Inject} from '@angular/core';
import {MD_DIALOG_DATA, MdDialogRef} from '@angular/material';

@Component({
  selector: 'app-filter-pop',
  templateUrl: './filter.pop.html',
  styleUrls: ['./filter.pop.scss']
})
export class FilterPop implements OnInit {
  filter = {
    isActive: false,
    crowd: {
      isActive: true,
      val: [0, 50]
    },
    gender: {
      isActive: true,
      val: 2
    },
    age: {
      isActive: true,
      val: [18, 45]
    },
  };



  constructor(
    @Inject(MD_DIALOG_DATA) public data: any,
    public dialogRef: MdDialogRef<FilterPop>
  ) {

  }

  ngOnInit() {
    this.filter = this.data;
  }

  crowdChange(e) {
    this.filter.crowd = e;
  }

  genderChange(e) {
    this.filter.gender = e;
  }

  ageChange(e) {
    this.filter.age = e;
  }

  closeSave() {
    this.filter.isActive = true;
    this.dialogRef.close(this.filter);
  }

  closeReset() {
    const reset = {
      isActive: false,
      crowd: {
        isActive: false,
        val: [0, 100]
      },
      gender: {
        isActive: false,
        val: [0],
      },
      age: {
        isActive: false,
        val: [0, 100]
      }
    };

    this.dialogRef.close(reset);
  }
}
