import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterPop } from './filter.pop';

describe('FilterPop', () => {
  let component: FilterPop;
  let fixture: ComponentFixture<FilterPop>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterPop ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterPop);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
