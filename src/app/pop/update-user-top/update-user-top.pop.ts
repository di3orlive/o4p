import {Component, Inject} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {MdDialog, MdDialogRef, MD_DIALOG_DATA, MdDialogConfig} from '@angular/material';
import {Md2Toast} from 'md2';
import {CustomValidators} from 'ng2-validation';
import * as sha256 from 'js-sha256/src/sha256.js';
import {SafeSubscribe} from '../../helpers/safe-subscripe/safe-subscripe';
import {ConfirmPop} from '../confirm/confirm.pop';
import {ApiService} from '../../services';

@Component({
  selector: 'app-update-user-top',
  templateUrl: './update-user-top.pop.html',
  styleUrls: ['./update-user-top.pop.scss']
})
export class UpdateUserTopPop extends SafeSubscribe {
  deleteConfirmDialogRef: MdDialogRef<ConfirmPop>;
  form: any;
  file = {
    arr: []
  };
  nationalities: any;
  nationalitiesCtrl: any;
  filteredNationalities: any;
  nationality_id: any;

  constructor(
    private api: ApiService,
    private translate: TranslateService,
    private router: Router,
    private toast: Md2Toast,
    private dialog: MdDialog,
    public dialogRef: MdDialogRef<UpdateUserTopPop>,
    @Inject(MD_DIALOG_DATA) public user: any
  ) {
    super();
    const username = new FormControl('', Validators.required);
    const fullname = new FormControl('');
    const password = new FormControl('');
    const password2 = new FormControl('', CustomValidators.equalTo(password));
    const description = new FormControl('');
    this.form = new FormGroup({
      username: username,
      fullname: fullname,
      password: password,
      password2: password2,
      description: description,
    });


    this.initUpdate(this.user);

    this.getNationalities();

    this.nationalitiesCtrl = new FormControl(this.user.nationality_label);
    this.filteredNationalities = this.nationalitiesCtrl.valueChanges.startWith(null).map(item => this.filterNationalities(item));
  }


  initUpdate(user) {
    this.form.controls['username'].setValue(user.username);
    this.form.controls['fullname'].setValue(user.fullname);
    this.form.controls['description'].setValue(user.description);

    this.file.arr.push({file: '', img: user.image, media_secure_id: user.medias[0].media_secure_id});
  }


  save() {
    if (this.file.arr.length < 1) {
      this.toast.toast(this.translate.instant('you_need_to_add_a_media_file'));
      return null;
    }

    if (this.form.invalid) { return null; }

    const body = {
      body: {
        description: this.form.value.description,
        username: this.form.value.username,
        fullname: this.form.value.fullname,
        password_hash: this.form.value.password ? sha256(this.form.value.password) : null,
        nationality_id: this.nationality_id
      },
      file: this.file
    };

    this.toast.toast(this.translate.instant('user_updating'));

    this.dialogRef.close(body);
  }


  onFileChange(e) {
    this.file.arr[0] = e.arr[0];
  }


  deleteConfirmPop() {
    this.deleteConfirmDialogRef = this.dialog.open(ConfirmPop, <MdDialogConfig>{
      data: {
        title: this.translate.instant('confirm_user_deleting')
      }
    });

    this.deleteConfirmDialogRef.afterClosed().safeSubscribe(this, res => {
      if (!!res) {
        this.api.deleteP(`users/${this.user.secure_id}`).safeSubscribe(this, () => {
          this.api.logOut();
          this.router.navigate(['']);
          this.toast.toast(this.translate.instant('user_deleted'));
          this.dialogRef.close();
        });
      }
    });
  }


  getNationalities() {
    this.api.getP('nationalities').safeSubscribe(this, (res: any) => {
      this.nationalities = res.nationalities;
    });
  }


  setNationalityId(id) {
    console.log(id);
    this.nationality_id = id;
  }


  filterNationalities(val) {
    return val ? this.nationalities.filter(s => s.label.toLowerCase().indexOf(val.toLowerCase()) === 0)
      : this.nationalities;
  }
}


