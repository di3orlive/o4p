import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateUserTopPop } from './update-user-top.pop';

describe('UpdateUserTopPop', () => {
  let component: UpdateUserTopPop;
  let fixture: ComponentFixture<UpdateUserTopPop>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateUserTopPop ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateUserTopPop);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
