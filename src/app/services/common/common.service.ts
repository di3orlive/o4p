import { Injectable } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {BehaviorSubject, Observable} from 'rxjs/Rx';
import * as introJs from 'intro.js/minified/intro.min.js'

@Injectable()
export class CommonService {
  introJs = introJs.introJs();
  
  api_port = window.location.hostname.startsWith('dev') || window.location.hostname.startsWith('localhost') ? 8082 : window.location.hostname.startsWith('beta') ? 8083 : window.location.hostname.startsWith('test') ? 8084 : 8081;
  api_url = `https://api.open4partiz.com:${this.api_port}/v2/`;
  socket_url = `https://api.open4partiz.com:${this.api_port}/`;
  
  
  progress = new BehaviorSubject<any>(0);

  options = new BehaviorSubject<any>(this.getDefaultOptions());

  O4PUser = new BehaviorSubject<any>({
    loggedIn: false,
    credit: 0,
    notifications: [],
    total_notifications: 0,
    user: {}
  });

  controls = new BehaviorSubject<any>({
    isPaused: false,
    isMuted: true
  });

  languages = new BehaviorSubject<any>({
    current: {},
    arr: []
  });

  map_subj = new BehaviorSubject<any>({
    map: null,
    mapService: null
  });
  mapPanBy: any;
  
  parameters = new BehaviorSubject<any>({});

  cities = [];


  constructor(
    private translate: TranslateService
  ) { }


  getCityByPlaceId(placeId) {
    const cities = localStorage.getItem('cities') ? JSON.parse(localStorage.getItem('cities')) : this.cities;

    return cities.filter(( obj ) => obj.placeId === placeId)[0];
  }

  pushCity(city) {
    this.cities.push(city);
    localStorage.removeItem('cities');
    localStorage.setItem('cities', JSON.stringify(this.cities));
  }


  get progressAsync() {
    return this.progress.asObservable().distinctUntilChanged();
  }

  setProgress(a) {
    this.progress.next(a);
  }


  get parametersAsync() {
    return this.parameters.asObservable().distinctUntilChanged();
  }

  setParameters(a) {
    this.parameters.next(a);
  }




  get optionsAsync() {
    return this.options.asObservable().distinctUntilChanged();
  }

  setOptions(a) {
    this.options.next(a);
  }

  getDefaultOptions() {
    return [
      {name: 'smoker', label: this.translate.instant('smoker_compliant'), isactive: false, pathWhite: false},
      {name: 'sleep', label: this.translate.instant('host_has_a_couch'), isactive: false, pathWhite: false},
      {name: 'wifi', label: this.translate.instant('youll_find_wifi_there'), isactive: false, pathWhite: false},
      {name: 'disabled', label: this.translate.instant('easy_access_for_disabled_people'), isactive: false, pathWhite: true},
    ];
  }




  get languagesAsync() {
    return this.languages.asObservable().distinctUntilChanged();
  }

  setLanguages(a) {
    this.languages.next(a);
    this.translate.use(a.current.id);
    localStorage.removeItem('O4PLang');
    localStorage.setItem('O4PLang', JSON.stringify(a.current.id));
  }




  get userAsync() {
    const a = localStorage.getItem('O4PUser') ? JSON.parse(localStorage.getItem('O4PUser')) : this.O4PUser.getValue();

    if (Object.keys(a.notifications).length === 0 && a.notifications.constructor === Object) {
      a.notifications = [];
    }
    a.total_notifications = 0;
    if (Object.getOwnPropertyNames(a.notifications).length > 0) {
      a.notifications.forEach((item) => {
        if (item.notif && item.notif.status !== 'displayed') {
          a.total_notifications++;
        }
      });
    }


    this.setUser(a);
    return this.O4PUser.asObservable().distinctUntilChanged();
  }

  setUser(a) {
    this.O4PUser.next(a);
    localStorage.removeItem('O4PUser');
    localStorage.setItem('O4PUser', JSON.stringify(this.O4PUser.getValue()));
  }

  resetUser() {
    this.O4PUser.next({
      loggedIn: false,
      credit: 0,
      notifications: [],
      total_notifications: 0,
      user: {}
    });
    localStorage.removeItem('O4PUser');
    localStorage.setItem('O4PUser', JSON.stringify(this.O4PUser.getValue()));
  }




  get controlsAsync() {
    return this.controls.asObservable().distinctUntilChanged();
  }

  toggleVideo() {
    this.controls.next({
      isPaused: !this.controls.getValue().isPaused,
      isMuted: this.controls.getValue().isMuted,
    });
  }

  toggleSound() {
    this.controls.next({
      isPaused: this.controls.getValue().isPaused,
      isMuted: !this.controls.getValue().isMuted,
    });
  }





  get mapAsync() {
    return this.map_subj.asObservable().distinctUntilChanged();
  }

  setMap(map, service) {
    this.map_subj.next({
      map: map,
      mapService: service
    });
  }

  getInfoFromPlaceId(placeId) {
    return new Observable((observer) => {
      this.map_subj.value.mapService.getDetails({placeId: placeId}, (response, status) => {
        if (placeId == null || !this.map_subj.value.mapService) {
          return Observable.empty();
        }


        if (status === 'OK') {
          const placeInfo = this.placeIdCityFilter(response.address_components);
          observer.next({
            city_country: placeInfo.city_country,
            city: placeInfo.city,
            country: placeInfo.country,
            full_address: response.formatted_address,
            placeId: placeId
          });
          observer.complete();
        } else if (status === 'OVER_QUERY_LIMIT') {
          setTimeout(() => {
            this.getInfoFromPlaceId(placeId);
          }, 1000);
        }
      });
    })
      .catch((err: any) => Observable.throw(err))
      .map((res) => res);
  }

  placeIdCityFilter(data): any {
    if (data) {
      const body = {
        city: '',
        country: '',
        city_country: ''
      };

      for (let i = 0; i < data.length; i++) {
        if (data[i].types[0] === 'locality' && !!data[i].long_name) {
          body.city = data[i].long_name;
        }
        if (data[i].types[0] === 'country' && !!data[i].long_name) {
          body.country = data[i].long_name;
        }
      }

      body.city_country = `${body.city} ${body.country}`;

      return body;
    }
  }
}




