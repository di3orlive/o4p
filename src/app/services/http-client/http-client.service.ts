import {Injectable} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { RequestOptions, Http, ConnectionBackend, Request, RequestOptionsArgs, Response } from '@angular/http';

@Injectable()
export class HttpClientService extends Http {

  constructor(
    _backend: ConnectionBackend,
    _defaultOptions: RequestOptions
  ) {
    super(_backend, _defaultOptions);
  }

  request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
    if (url instanceof Request) {
      const O4PUser = localStorage.getItem('O4PUser') ? JSON.parse(localStorage.getItem('O4PUser')) : false;
      
      url.headers.set('Content-Type', 'application/json');
      url.headers.set('Accept', 'application/json');
  
  
      if (O4PUser) {
        url.headers.set('Accept-Language', O4PUser.curLang);
        
        if (O4PUser.sessionToken) {
          url.headers.set('Authorization', O4PUser.sessionToken);
        }
      }
    }
    return super.request(url, options);
  }
}


