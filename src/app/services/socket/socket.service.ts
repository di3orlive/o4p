import {Injectable} from '@angular/core';
import * as io from 'socket.io-client';
import {Observable} from 'rxjs/Observable';
import {CommonService} from '../common/common.service';
import {Md2Toast} from 'md2';

@Injectable()
export class SocketService {
  socket: any;
  O4PUser: any;


  constructor(
    private commonService: CommonService, private toast: Md2Toast
  ) {
    this.commonService.userAsync.subscribe((res) => {
      this.O4PUser = res;
    });

  }

  init() {
    this.socket = io(this.commonService.socket_url);

    this.socket.on('token', (res) => {
      if (this.O4PUser.sessionToken && res === 'send') {
        this.socket.emit('token', {token: this.O4PUser.sessionToken});
      }
    });
    
    this.socket.on('toast', (res) => {
    console.log(res);
				this.toast.toast(res);
    });
    
    
  }

  onNoti() {
    return new Observable(observer => {
      this.socket.on('notification', (data) => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
  }
  
		
}












