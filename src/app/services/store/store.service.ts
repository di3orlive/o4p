import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class StoreService {
  store = new BehaviorSubject<any>('');
  changes = this.store.asObservable().distinctUntilChanged();

  setState(state) {
    this.store.next(state);
  }


  getState() {
    return this.store.value;
  }


  purge() {
    this.store.next([]);
  }

}
