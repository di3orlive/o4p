import { Injectable } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {Router} from '@angular/router';
import {Md2Toast} from 'md2';
import {FacebookService} from 'ngx-facebook';
import {CommonService} from '../common/common.service';
import {StoreHelperService} from '../store-helper/store-helper.service';
import {MdDialog} from '@angular/material';


@Injectable()
export class ApiService {
  O4PUser: any;

  constructor(
    private toast: Md2Toast,
    private storeHelper: StoreHelperService,
    private translate: TranslateService,
    private commonService: CommonService,
    private router: Router,
    private fb: FacebookService,
    public dialog: MdDialog,
    private http: Http
  ) {

    this.commonService.userAsync.subscribe((value) => {
      this.O4PUser = value;
    });
  }


  checkParams(obj) {
    return JSON.parse(JSON.stringify(obj,
      function(k, v) { if (v === null) { return undefined; } return v; }
    ));
  }


  checkForErr(err) {
    const body = (typeof err._body) === 'string' ? JSON.parse(err._body) : err._body;
  
  
    console.log(body);
  
    if (body) {
      // if (body.success) {
      //
      // }
  
      this.toast.toast(body.statusText);
  
      if (body.sessionExpired) {
        this.logOut();
      }
      
      if (body.toHomePage) {
        this.router.navigate(['']);
      }
    }

    return Observable.throw(err).catch(err => Observable.of(err));
  }



// =====================================================================================================================

  getP(path) {
    return this.http.get(`${this.commonService.api_url}${path}`)
      .map((res) => res.json())
      .catch((err: any) => this.checkForErr(err));
  }

  getPB(path, body) {
    return this.http.get(`${this.commonService.api_url}${path}`, {params: this.checkParams(body)})
      .map((res) => res.json())
      .catch((err: any) => this.checkForErr(err));
  }

  postPB(path, body) {
    return this.http.post(`${this.commonService.api_url}${path}`, this.checkParams(body))
      .map((res) => res.json())
      .catch((err: any) => this.checkForErr(err));
  }

  putPB(path, body) {
    return this.http.put(`${this.commonService.api_url}${path}`, this.checkParams(body))
      .map((res) => res.json())
      .catch((err: any) => this.checkForErr(err));
  }

  deleteP(path) {
    return this.http.delete(`${this.commonService.api_url}${path}`)
      .map((res) => res.json())
      .catch((err: any) => this.checkForErr(err));
  }

  uploadB(body) {
    return new Observable((observer: any) => {
      const xhr = new XMLHttpRequest();

      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            observer.next(JSON.parse(JSON.stringify(xhr.response)));
            observer.complete();
            this.commonService.setProgress(0);
          } else {
            observer.error(xhr.response);
          }
        }
      };

      xhr.upload.onprogress = (e) => {
        this.commonService.setProgress((e.loaded * 100) / e.total);
      };


      xhr.open('POST', `${this.commonService.api_url}upload`, true);
      xhr.setRequestHeader('Authorization', this.O4PUser.sessionToken);
      xhr.send(body);
    })
      .catch((err: any) => this.checkForErr(err))
      .map((res) => JSON.parse(res));
  }

// =====================================================================================================================

  logOut() {
    if (!this.O4PUser.loggedIn) { return null; }


    this.deleteP(`sessions/${this.O4PUser.sessionToken}`).subscribe(() => {

      if (this.O4PUser.sessionType === 'fbSession') {
        this.fb.logout().then(() => {
          this.toast.toast(this.translate.instant('logged_out'));

          this.router.navigate(['']);

          this.commonService.resetUser();
          return null;
        });
      }

      this.router.navigate(['']);
      this.commonService.resetUser();
    });
  }

  getUser(secure_id) {
    return this.http.get(`${this.commonService.api_url}users/${secure_id}`)
      .map((res) => res.json())
      .catch((err: any) => this.checkForErr(err))
      .do((res) => this.storeHelper.update('user', res));
  }

  getEvent(secure_id) {
    return this.http.get(`${this.commonService.api_url}events/${secure_id}`).catch((err: any) => {
      this.toast.toast(JSON.parse(JSON.stringify(err._body)).statusText);
      return Observable.throw(err);
    })
      .map((res) => res.json())
      .catch((err: any) => this.checkForErr(err))
      .do((res) => this.storeHelper.update('event', res));
  }

  getEvents(body) {
    return this.http.get(`${this.commonService.api_url}events`, {params: this.checkParams(body)}).catch((err: any) => {
      this.toast.toast(JSON.parse(JSON.stringify(err._body)).statusText);
      return Observable.throw(err);
    })
      .map((res) => res.json())
      .catch((err: any) => this.checkForErr(err))
      .do((res) => this.storeHelper.update('events', res));
  }

// =====================================================================================================================

  getAttendeeList(path) {
    return this.http.get(`${this.commonService.api_url}${path}`)
      .catch((err: any) => this.checkForErr(err))
      .map((res) => res);
  }
  
  getParticipationCard(path) {
    return this.http.get(`${this.commonService.api_url}${path}`)
      .map((res) => res)
      .catch((err: any) => this.checkForErr(err));
  }

// =====================================================================================================================
  
  getNotifications() {
    const a = this.commonService.O4PUser.getValue();
    
    if (a.sessionToken) {
      this.getPB(`notifications`, {page_nb: 0}).subscribe((res: any) => {
        a.notifications = res.notifications;
        a.total_notifications = 0;
        if (Object.getOwnPropertyNames(a.notifications).length > 0) {
          a.notifications.forEach((item) => {
            if (item.notif && item.notif.status !== 'displayed') {
              a.total_notifications++;
            }
          });
        }
        this.commonService.setUser(a);
      });
    }
  }
}

