import {style, animate, transition, state, trigger} from '@angular/animations';

export class Animations {
  static page = [
    trigger('routeAnimation', [
      state('*', style({opacity: 1})),
      transition('void => *', [
        style({transform: 'translateY(-10px)', opacity: 0}), animate('300ms linear')
      ]),
      transition('* => void',
        style({display: 'none'})
      )
    ])
  ];
}
