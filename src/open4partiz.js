var connect = require('connect');
var serveStatic = require('serve-static');
var compress = require('compression');
var http = require('http');
//var https = require('https');
var fs = require('fs');
var auth = require('http-auth');
var basic = auth.basic({
    realm: "O4P beta",
    file: __dirname + "/.htpasswd"
});
var mode;
if(typeof process.argv[2] != 'undefined')
{
	mode = process.argv[2];
	console.log('Mode was set via arguments, starting as : ' + mode);
}
else if(fs.existsSync('./mode.txt'))
{
	console.log('Mode was set via file, starting as : ' + mode);
	mode = fs.readFileSync('./mode.txt', 'utf8');
}
else
{
	console.log('Node mode was set, exiting');
	process.exit(1);
}
if(typeof mode == 'undefined' || mode != 'dev' && mode != 'prod' && mode != 'beta' && mode != 'test')
{
	console.log('Invalid mode, exiting');
	process.exit(1);
}

var app = connect();

app.use(compress());
//app.use(auth.connect(basic));


var favicon = require('serve-favicon');

/*
var https_options = {
 ca: fs.readFileSync("/home/node/ssl/intermediate.crt"),
 key: fs.readFileSync("/home/node/ssl/private.key"),
 cert: fs.readFileSync("/home/node/ssl/open4partiz.crt")
};
*/

app.use(favicon(__dirname + '/favicon.ico'));

app.use(serveStatic(__dirname, {'index': ['index.html']}));

var ui_port;
if(mode == 'prod')
{
	ui_port = 1337;
}
else if(mode == 'dev')
{
	ui_port = 1338;
}
else if(mode == 'beta')
{
	ui_port = 1339;
}
else if(mode == 'test')
{
	ui_port = 1340;
}
console.log('The server is starting on port ' + ui_port);
app.listen(ui_port);
//https.createServer(https_options, app).listen(443);
