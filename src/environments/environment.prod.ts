export const environment = {
  apiUrl: 'https://api.open4partiz.com:8082/v2/',
  socketUrl: 'https://api.open4partiz.com:8082/',
  production: true,
  hmr: false
};
